package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.ValidateTools;
import cn.cliveyuan.tools.common.bean.ValidationResult;
import cn.cliveyuan.tools.common.test.bean.User;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author clive
 * Created on 2018/07/24
 * @since 1.0
 */
public class ValidateToolsTest {

    @Test
    public void isName() {
        String str = "坎坎坷坷解决李四在干嘛你哈哈哈";
        Assert.assertTrue(ValidateTools.isName(str));
    }

    @Test
    public void isFloat() {
        String str = "-8";
        Assert.assertTrue(ValidateTools.isFloat(str));
    }

    @Test
    public void isUrl() {
        String str = "http://www.baidu.com";
        Assert.assertTrue(ValidateTools.isUrl(str));
    }

    @Test
    public void isMobile() {
        String str = "13800138000";
        Assert.assertTrue(ValidateTools.isMobile(str));
    }

    @Test
    public void isEmail() {
        String str = "cliveyuan@foxmail.com";
        Assert.assertTrue(ValidateTools.isEmail(str));
    }

    @Test
    public void isBankCardNo() {
        String str = "6217002710000684874";//网上随便找的
        Assert.assertTrue(ValidateTools.isBankCardNo(str));
    }

    @Test
    public void isIdCardNo() {
        //网上随机生成的
        String ids = "110101199003073693\n" +
                "110101199003072973\n" +
                "110101199003077694\n" +
                "110101199003075218\n" +
                "110101199003078179\n" +
                "110101199003072834\n" +
                "110101199003079956\n" +
                "110101199003078750\n" +
                "110101199003077715\n" +
                "110101199003077352\n" +
                "110101199003074653\n" +
                "11010119900307141X\n" +
                "110101199003073570\n" +
                "110101199003074151\n" +
                "110101199003077336\n" +
                "110101199003072295\n" +
                "110101199003077336\n" +
                "110101199003070193\n" +
                "110101199003078312\n" +
                "11010119900307635X\n" +
                "110101199003076675\n" +
                "110101199003073650\n" +
                "110101199003076499\n" +
                "110101199003071698\n" +
                "11010119900307547X\n" +
                "11010119900307141X\n" +
                "110101199003074493\n" +
                "110101199003070038\n" +
                "110101199003074290\n" +
                "110101199003076173\n" +
                "11010119900307467x";
        String[] lines = ids.split("\n");
        for (String line : lines) {
            Assert.assertTrue(ValidateTools.isIdCardNo(line));
        }
    }

    @Test
    public void testIp() {
        Assert.assertTrue(ValidateTools.isIp("192.168.0.1"));
        Assert.assertTrue(ValidateTools.isIp("1.168.0.1"));
        Assert.assertTrue(ValidateTools.isIp("117.24.12.245"));
        Assert.assertFalse(ValidateTools.isIp("0.168.0.1"));
        Assert.assertFalse(ValidateTools.isIp("www.cliveyuan.cn"));
        Assert.assertFalse(ValidateTools.isIp("192.168.0.900"));
        Assert.assertFalse(ValidateTools.isIp("256.168.0.1"));
        Assert.assertFalse(ValidateTools.isIp("http://117.24.12.245:8888/"));
        Assert.assertFalse(ValidateTools.isIp("117.24.12.245:8888"));
    }

    @Test
    public void test_assertValidateEntity() {
        User user = new User();
        try {
            ValidateTools.assertValidateEntity(user);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("name: must not be blank", e.getMessage());
        }
        try {
            ValidateTools.assertValidateEntityWithoutFieldName(user);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("must not be blank", e.getMessage());
        }
    }
}
