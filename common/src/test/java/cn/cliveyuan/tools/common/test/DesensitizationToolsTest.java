package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.DesensitizationTools;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Clive Yuan
 * @date 2022/06/25
 */
public class DesensitizationToolsTest {
    @Test
    public void testFormatMobile() {
        Assert.assertEquals("188****0111",DesensitizationTools.formatMobile("18817800111"));
    }

    @Test
    public void testFormatIdCardNo() {
        Assert.assertEquals("51018**********888",DesensitizationTools.formatIdCardNo("510184199401018888"));
    }

    @Test
    public void testFormatName() {
        Assert.assertEquals("",DesensitizationTools.formatName(""));
        Assert.assertEquals("张*",DesensitizationTools.formatName("张三"));
        Assert.assertEquals("张*丰",DesensitizationTools.formatName("张三丰"));
        Assert.assertEquals("张",DesensitizationTools.formatName("张"));
        Assert.assertEquals("王**子",DesensitizationTools.formatName("王二麻子"));
        Assert.assertEquals("尼*****奇",DesensitizationTools.formatName("尼古拉斯·凯奇"));
    }
}
