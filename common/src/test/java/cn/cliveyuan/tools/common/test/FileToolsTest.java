package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.FileTools;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author: clive
 * Created on 2018/07/23
 * @since: 1.0
 */
public class FileToolsTest {

    @Test
    public void readFileToStringList() throws IOException {
        String rootPath = FileToolsTest.class.getResource("/").getPath();
        String pathname = rootPath + "/read_test.txt";
        File file = new File(pathname);
        FileTools.writeStringToFile(file, "a\nb\nc", StandardCharsets.UTF_8);
        List<String> lines = FileTools.readFileToStringList(file);
        lines.forEach(System.out::println);
        System.out.println("size=" + lines.size());
        Assert.assertEquals(lines.size(), 3);
    }
}
