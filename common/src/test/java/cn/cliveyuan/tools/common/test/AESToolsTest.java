package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.AESTools;
import cn.cliveyuan.tools.common.AssertTools;
import cn.cliveyuan.tools.common.bean.aes.AesRequest;
import cn.cliveyuan.tools.common.bean.aes.AesResponse;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;

import java.security.Security;

/**
 * @author clive®
 * Created on 2018/07/24
 * @since 1.0
 */
public class AESToolsTest {

    private static final String AES_KEY = "1234567887654321";
    private static final String PLAINTEXT = "欢迎使用 java-tools";
    private static final String BASE64_CIPHER_TEXT = "wYVOg69loeuw0XTZ3rQ1uAmkjl+Sn7lQQN9Dinmop5E=";
    private static final String HEX_CIPHER_TEXT = "c1854e83af65a1ebb0d174d9deb435b809a48e5f929fb95040df438a79a8a791";

    @Test
    public void encryptBase64() {
        String encrypt = AESTools.encryptBase64(AES_KEY, PLAINTEXT);
        System.out.println(encrypt);
        Assert.assertEquals(encrypt, BASE64_CIPHER_TEXT);
    }

    @Test
    public void decryptBase64() {
        String decrypt = AESTools.decryptBase64(AES_KEY, BASE64_CIPHER_TEXT);
        System.out.println(decrypt);
        Assert.assertEquals(decrypt, PLAINTEXT);
    }

    @Test
    public void encryptHex() {
        String encrypt = AESTools.encryptHex(AES_KEY, PLAINTEXT);
        System.out.println(encrypt);
        Assert.assertEquals(encrypt, HEX_CIPHER_TEXT);
    }

    @Test
    public void decryptHex() {
        String decrypt = AESTools.decryptHex(AES_KEY, HEX_CIPHER_TEXT);
        System.out.println(decrypt);
        Assert.assertEquals(decrypt, PLAINTEXT);
    }

    @Test
    public void aesProvider() {
        String base64CipherText = "Ww+fk7Qak7TFDhtel2uPodr6BrrtDjIe9c7WwkJHcmQ=";
        String provider = "BC";
        String transformation = "AES/CBC/PKCS7Padding";
        Security.addProvider(new BouncyCastleProvider());

        AesResponse encryptResponse = AESTools.encrypt(AesRequest.builder()
                .transformation(transformation)
                .key(AES_KEY.getBytes())
                .iv(AES_KEY.getBytes())
                .data(PLAINTEXT.getBytes())
                .provider(provider)
                .build());
        System.out.println(encryptResponse);
        Assert.assertTrue(encryptResponse.getMsg(), encryptResponse.isSuccess());
        Assert.assertEquals(base64CipherText, Base64.encodeBase64String(encryptResponse.getData()));

        AesResponse decryptResponse = AESTools.decrypt(AesRequest.builder()
                .transformation(transformation)
                .key(AES_KEY.getBytes())
                .iv(AES_KEY.getBytes())
                .data(encryptResponse.getData())
                .provider(provider)
                .build());
        System.out.println(decryptResponse);
        Assert.assertTrue(decryptResponse.getMsg(), decryptResponse.isSuccess());
        Assert.assertEquals(PLAINTEXT, new String(decryptResponse.getData()));
    }
}
