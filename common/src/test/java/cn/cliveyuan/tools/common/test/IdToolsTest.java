package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.IdTools;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @author clive
 * Created on 2018/07/27
 * @since 1.0
 */
public class IdToolsTest {
    @Test
    public void genSnowflakeId() {
        Set<String> set = new HashSet<>();
        int count = 10000;
        for (int i = 0; i < count; i++) {
            String no = IdTools.genSnowflakeId();
            System.out.println(no);
            set.add(no);
        }
        Assert.assertEquals(count, set.size());
    }
}
