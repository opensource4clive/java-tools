package cn.cliveyuan.tools.common.test.bean;

import cn.cliveyuan.tools.common.annotation.XmlField;
import lombok.Data;

/**
 * @author Clive Yuan
 * @date 2021/04/09
 */
@Data
public class WechatTransferResponse {
    @XmlField("return_code")
    private String returnCode;
    @XmlField("return_msg")
    private String returnMsg;
    @XmlField("err_code")
    private String errCode;
    @XmlField("err_code_des")
    private String errCodeDes;
    @XmlField("mch_appid")
    private String mchAppid;
    @XmlField("mchid")
    private String mchid;
    @XmlField("device_info")
    private String deviceInfo;
    @XmlField("nonce_str")
    private String nonceStr;
    @XmlField("result_code")
    private String resultCode;
    @XmlField("partner_trade_no")
    private String partnerTradeNo;
    @XmlField("payment_no")
    private String paymentNo;
    @XmlField("payment_time")
    private String paymentTime;
}
