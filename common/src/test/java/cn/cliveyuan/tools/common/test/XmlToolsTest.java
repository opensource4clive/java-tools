package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.XmlTools;
import cn.cliveyuan.tools.common.test.bean.WechatTransferRequest;
import cn.cliveyuan.tools.common.test.bean.WechatTransferResponse;
import org.junit.Test;

/**
 * @author Clive Yuan
 * @date 2022/06/25
 */
public class XmlToolsTest {

    @Test
    public void parseObject() {
        String xml = "<xml>\n" +
                "\n" +
                "<return_code><![CDATA[SUCCESS]]></return_code>\n" +
                "\n" +
                "<return_msg><![CDATA[]]></return_msg>\n" +
                "\n" +
                "<mch_appid><![CDATA[wxec38b8ff840bd989]]></mch_appid>\n" +
                "\n" +
                "<mchid><![CDATA[10013274]]></mchid>\n" +
                "\n" +
                "<device_info><![CDATA[]]></device_info>\n" +
                "\n" +
                "<nonce_str><![CDATA[lxuDzMnRjpcXzxLx0q]]></nonce_str>\n" +
                "\n" +
                "<result_code><![CDATA[SUCCESS]]></result_code>\n" +
                "\n" +
                "<partner_trade_no><![CDATA[10013574201505191526582441]]></partner_trade_no>\n" +
                "\n" +
                "<payment_no><![CDATA[1000018301201505190181489473]]></payment_no>\n" +
                "\n" +
                "<payment_time><![CDATA[2015-05-19 15：26：59]]></payment_time>\n" +
                "\n" +
                "</xml>";
        WechatTransferResponse wechatTransferResponse = XmlTools.parseObject(xml, WechatTransferResponse.class);
        System.out.println(wechatTransferResponse);
    }

    @Test
    public void toXmlString() {
        WechatTransferRequest wechatTransferRequest = new WechatTransferRequest();
        wechatTransferRequest.setMchAppid("wxe062425f740c30d8");
        wechatTransferRequest.setMchid("10000098");
        wechatTransferRequest.setNonceStr("3PG2J4ILTKCH16CQ2502SI8ZNMTM67VS");
        wechatTransferRequest.setPartnerTradeNo("100000982014120919616");
        wechatTransferRequest.setOpenid("ohO4Gt7wVPxIT1A9GjFaMYMiZY1s");
        wechatTransferRequest.setCheckName("FORCE_CHECK");
        wechatTransferRequest.setReUserName("张三");
        wechatTransferRequest.setAmount("100");
        wechatTransferRequest.setDesc("节日快乐!");
        wechatTransferRequest.setSign("C97BDBACF37622775366F38B629F45E3");
        String xml = XmlTools.toXmlString(wechatTransferRequest);
        System.out.println(xml);
    }
}
