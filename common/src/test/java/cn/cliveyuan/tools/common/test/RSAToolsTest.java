package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.RSATools;
import cn.cliveyuan.tools.common.bean.rsa.RSAKeyPair;
import cn.cliveyuan.tools.common.bean.rsa.RsaRequest;
import cn.cliveyuan.tools.common.bean.rsa.RsaResponse;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;

import java.security.Security;

/**
 * @author Clive Yuan
 * @date 2021/09/03
 */
public class RSAToolsTest {

    private static final String PLAINTEXT = "欢迎使用 java-tools";

    @Test
    public void testEncryptAndDecrypt() {
        RSAKeyPair rsaKeyPair = RSATools.generateKeyPair();
        String encrypt = RSATools.encrypt(PLAINTEXT, rsaKeyPair.getPublicKey());
        System.out.println(encrypt);
        String decrypt = RSATools.decrypt(encrypt, rsaKeyPair.getPrivateKey());
        System.out.println(decrypt);
        Assert.assertEquals(PLAINTEXT, decrypt);
    }

    @Test
    public void testSignAndVerifySign() {
        RSAKeyPair rsaKeyPair = RSATools.generateKeyPair();
        String sign = RSATools.sign(PLAINTEXT, rsaKeyPair.getPrivateKey());
        System.out.println(sign);
        Assert.assertTrue(RSATools.verifySign(PLAINTEXT, sign, rsaKeyPair.getPublicKey()));
    }

    @Test
    public void rsaProvider() {
        RSAKeyPair rsaKeyPair = RSATools.generateKeyPair();
        String provider = "BC";
        String cipherAlgorithm = "RSA/None/PKCS1Padding";
        Security.addProvider(new BouncyCastleProvider());

        RsaResponse encryptResponse = RSATools.encrypt(RsaRequest.builder()
                .cipherAlgorithm(cipherAlgorithm)
                .key(Base64.decodeBase64(rsaKeyPair.getPublicKey()))
                .data(PLAINTEXT.getBytes())
                .provider(provider)
                .build());
        System.out.println(encryptResponse);
        Assert.assertTrue(encryptResponse.getMsg(), encryptResponse.isSuccess());

        RsaResponse decryptResponse = RSATools.decrypt(RsaRequest.builder()
                .cipherAlgorithm(cipherAlgorithm)
                .key(Base64.decodeBase64(rsaKeyPair.getPrivateKey()))
                .data(encryptResponse.getData())
                .provider(provider)
                .build());
        System.out.println(decryptResponse);
        Assert.assertTrue(decryptResponse.getMsg(), decryptResponse.isSuccess());
        Assert.assertEquals(PLAINTEXT, new String(decryptResponse.getData()));
    }
}
