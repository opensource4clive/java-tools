package cn.cliveyuan.tools.common.test.bean;

import cn.cliveyuan.tools.common.annotation.XmlField;
import lombok.Data;

/**
 * @author Clive Yuan
 * @date 2021/04/09
 */
@Data
public class WechatTransferRequest {
    @XmlField("mch_appid")
    private String mchAppid;
    private String mchid;
    @XmlField("nonce_str")
    private String nonceStr;
    @XmlField("partner_trade_no")
    private String partnerTradeNo;
    private String openid;
    @XmlField("check_name")
    private String checkName;
    @XmlField("re_user_name")
    private String reUserName;
    /**
     * 单位:分
     */
    private String amount;
    private String desc;
    private String sign;
}
