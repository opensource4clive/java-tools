package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.ObjectTools;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Clive Yuan
 * @date 2022/06/25
 */
public class ObjectToolsTest {
    @Test
    public void getOrDefault() {
        Assert.assertEquals("java", ObjectTools.getOrDefault(null, "java"));
        Assert.assertEquals(1, ObjectTools.getOrDefault(1, 2).intValue());
        Assert.assertEquals(2L, ObjectTools.getOrDefault(null, 2L).longValue());
        Assert.assertEquals(false, ObjectTools.getOrDefault(null, false));
        Assert.assertEquals(true, ObjectTools.getOrDefault(true, false));
        Assert.assertEquals("true", ObjectTools.getOrDefaultString("true", "false"));
        Assert.assertEquals("false", ObjectTools.getOrDefaultString("   ", "false"));
        Assert.assertEquals("", ObjectTools.getOrDefaultString(null));
    }
}
