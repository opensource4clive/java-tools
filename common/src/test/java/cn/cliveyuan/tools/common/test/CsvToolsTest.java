package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.CsvTools;
import cn.cliveyuan.tools.common.DateTimeTools;
import cn.cliveyuan.tools.common.bean.csv.CsvReader;
import cn.cliveyuan.tools.common.bean.csv.CsvWriter;
import cn.cliveyuan.tools.common.test.bean.Person;
import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author Clive Yuan
 * @date 2020/06/22
 */
public class CsvToolsTest {

    @Test
    public void writeAndRead() {
        String[] headers = {"姓名", "性别", "年龄", "生日", "身高", "体重", "职位", "国籍", "活了多少秒", "是程序员吗", "月薪"};
        List<Person> data = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            data.add(Person.build()
                    .Name("张三" + i)
                    .Gender("男")
                    .Age(25 + i)
                    .Birthday(DateTimeTools.now())
                    .Height(1.85)
                    .Weight(90 + i)
                    .Position("攻城狮")
                    .Nation("中国")
                    .LiveSecond(1090909090 + i)
                    .Programmer(true)
                    .Salary(BigDecimal.valueOf(28000 + i))
            );
        }

        String filePath = CsvToolsTest.class.getResource("/").getPath();

        File generatedFile = CsvTools.write(CsvWriter.<Person>builder()
                .headers(headers)
                .pathname(filePath)
                .data(data)
                .build());
        Assert.assertNotNull(generatedFile);
        System.out.println("generatedFile:" + generatedFile.getAbsolutePath());

        List<Person> parseResult = CsvTools.read(CsvReader.<Person>builder()
                .filePath(generatedFile.getAbsolutePath())
                .clazz(Person.class)
                .build());
        System.out.println(parseResult);
        Assert.assertFalse(parseResult.isEmpty());
    }
}
