package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.ThreadTools;

/**
 * @author clive
 * Created on 2018/11/01
 * @since 1.0
 */
public class ThreadToolsTest {

    public static void main(String[] args) {
        System.out.println("main start");
        ThreadTools.async(() -> {
            System.out.println("start");
            ThreadTools.sleep(10);
            System.out.println("end");
        });
        System.out.println("main end");
    }
}
