package cn.cliveyuan.tools.common.test.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 测试对象: 人
 *
 * @author clive
 * Created on 2018/07/25
 * @since 1.0
 */
public class Person {

    private String name;
    private String gender;
    private int age;
    private Date birthday;
    private double height;
    private float weight;
    private String position;
    private String nation;
    private long liveSecond;
    private boolean programmer;
    private BigDecimal salary;

    public static Person build() {
        return new Person();
    }

    public String getName() {
        return name;
    }

    public Person Name(String name) {
        this.name = name;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public Person Gender(String gender) {
        this.gender = gender;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Person Age(int age) {
        this.age = age;
        return this;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Person Birthday(Date birthday) {
        this.birthday = birthday;
        return this;
    }

    public double getHeight() {
        return height;
    }

    public Person Height(double height) {
        this.height = height;
        return this;
    }

    public float getWeight() {
        return weight;
    }

    public Person Weight(float weight) {
        this.weight = weight;
        return this;
    }

    public String getPosition() {
        return position;
    }

    public Person Position(String position) {
        this.position = position;
        return this;
    }

    public String getNation() {
        return nation;
    }

    public Person Nation(String nation) {
        this.nation = nation;
        return this;
    }

    public long getLiveSecond() {
        return liveSecond;
    }

    public Person LiveSecond(long liveSecond) {
        this.liveSecond = liveSecond;
        return this;
    }

    public boolean isProgrammer() {
        return programmer;
    }

    public Person Programmer(boolean programmer) {
        this.programmer = programmer;
        return this;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public Person Salary(BigDecimal salary) {
        this.salary = salary;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public void setLiveSecond(long liveSecond) {
        this.liveSecond = liveSecond;
    }

    public void setProgrammer(boolean programmer) {
        this.programmer = programmer;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                ", height=" + height +
                ", weight=" + weight +
                ", position='" + position + '\'' +
                ", nation='" + nation + '\'' +
                ", liveSecond=" + liveSecond +
                ", programmer=" + programmer +
                ", salary=" + salary +
                '}';
    }
}
