package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.StringTools;
import cn.cliveyuan.tools.common.test.bean.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: clive
 * Created on 2018/07/23
 * @since: 1.0
 */
public class StringToolsTest {

    @Test
    public void stringToUnicode() {
        String s = "中文";
        String unicode = StringTools.stringToUnicode(s);
        System.out.println(unicode);
        Assert.assertEquals(unicode, "\\u4e2d\\u6587");
    }

    @Test
    public void unicodeToString() {
        String unicode = "\\u4e2d\\u6587";
        String str = StringTools.unicodeToString(unicode);
        System.out.println(str);
        Assert.assertEquals(str, "中文");
    }

    @Test
    public void substringIfOverflow() {
        String s = "测试字符串超出截断";
        Assert.assertEquals("测试", StringTools.substringIfOverflow(s, 2));
    }

    @Test
    public void str2Map() {
        String str = "user_clive\n" +
                "pwd: xxx_view";
        System.out.println(str);
        Map<String, String> map = StringTools.str2Map(str);
        System.out.println(map);
    }

    @Test
    public void between() {
        String value = "第8页,共10条";
        String[] between = StringTools.between(value, "共", "条");
        for (String s : between) {
            System.out.println(s);
        }
    }

    @Test
    public void firstBetween() {
        String value = "第8页,共10条";
        String result = StringTools.firstBetween(value, "第", "页");
        System.out.println(result);
    }

    @Test
    public void map2QueryStr() {
        Map<String, String> map = new HashMap<>();
        map.put("a", "1");
        map.put("b", "2");
        map.put("c", "3");
        System.out.println(StringTools.map2QueryStr(map));
    }

    @Test
    public void queryStr2Map() {
        String query = "a=1&b=2&c=&d";
        System.out.println(StringTools.queryStr2Map(query));
    }

    @Test
    public void format() {
        String template = "log: s={},i={},obj={}";
        String s = "Clive";
        int i = 100;
        Person person = Person.build();
        person.setName("Tina");
        person.setAge(18);
        person.setGender("girl");
        System.out.println(StringTools.format(template, s, i, person));
    }

    @Test
    public void substring() {
        String s = "0123456789";
        Assert.assertEquals("56789", StringTools.substring(s, 5));
        Assert.assertEquals("12", StringTools.substring(s, 1, 3));
    }

    @Test
    public void substringLength() {
        String s = "0123456789";
        Assert.assertEquals("01234", StringTools.substringLength(s, 5));
        Assert.assertEquals("1", StringTools.substringLength(s, 1, 1));
    }
}
