package cn.cliveyuan.tools.common.test.bean;

import lombok.Data;

import jakarta.validation.constraints.NotBlank;

/**
 * @author Clive Yuan
 * @date 2021/09/29
 */
@Data
public class User {
    @NotBlank
    private String name;
}
