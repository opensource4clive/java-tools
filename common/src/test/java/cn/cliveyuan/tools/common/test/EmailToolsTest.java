package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.EmailTools;
import cn.cliveyuan.tools.common.bean.EmailParam;

import java.util.Collections;

/**
 * @author clive
 * Created on 2018/07/24
 * @since 1.0
 */
public class EmailToolsTest {

    // https://gitee.com/cliveyuan/bid/blob/master/src/main/resources/config/bid.properties

    private static final String HOST = "smtp.exmail.qq.com";
    private static final String ACCOUNT = "system@cliveyuan.cn";
    private static final String PASSWORD = "***";

    // @Test
    public void send()  {
       EmailTools.send(EmailParam.builder()
               .host(HOST)
               .account(ACCOUNT)
               .password(PASSWORD)
               .recipients(Collections.singletonList("cliveyuan@foxmail.com"))
               .subject("Email Tools Test")
               .content("Hello World!")
               .build());
    }

   // @Test
    public void sendBySSL()  {
       EmailTools.send(EmailParam.builder()
               .host(HOST)
               .account(ACCOUNT)
               .password(PASSWORD)
               .recipients(Collections.singletonList("cliveyuan@foxmail.com"))
               .subject("ssl Email Tools Test")
               .content("Hello World!")
               .isSSL(true)
               .build());
    }
}
