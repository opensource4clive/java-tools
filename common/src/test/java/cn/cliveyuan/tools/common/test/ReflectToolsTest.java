package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.ReflectTools;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author Clive Yuan
 * @date 2022/06/25
 */
public class ReflectToolsTest {
    private String test;

    @Test
    public void getDeclaredFields() {
        List<Field> declaredFields = ReflectTools.getDeclaredFields(ReflectToolsTest.class);
        System.out.println(declaredFields);
        Assert.assertTrue(declaredFields.stream().anyMatch(x -> x.getName().equals("test")));
    }

    @Test
    public void getDeclaredMethods() {
        List<Method> declaredFields = ReflectTools.getDeclaredMethods(ReflectToolsTest.class);
        System.out.println(declaredFields);
        Assert.assertTrue(declaredFields.stream().anyMatch(x -> x.getName().equals("getDeclaredMethods")));
    }
}
