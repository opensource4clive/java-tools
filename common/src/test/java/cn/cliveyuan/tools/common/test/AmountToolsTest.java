package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.AmountTools;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * @author Clive Yuan
 * @date 2022/06/25
 */
public class AmountToolsTest {

    @Test
    public void yuan2Fen() {
        Assert.assertEquals(1024, AmountTools.yuan2Fen(new BigDecimal("10.24")).intValue());
    }

    @Test
    public void fen2Yuan() {
        Assert.assertEquals(0, new BigDecimal("20.48").compareTo(AmountTools.fen2Yuan(2048)));
    }

    @Test
    public void getPositiveAmt() {
        Assert.assertEquals(0, BigDecimal.ZERO.compareTo(AmountTools.getPositiveAmt(null)));
        Assert.assertEquals(0, BigDecimal.ZERO.compareTo(AmountTools.getPositiveAmt(new BigDecimal("-1"))));
        Assert.assertEquals(0, new BigDecimal("100").compareTo(AmountTools.getPositiveAmt(new BigDecimal("100"))));
    }
}
