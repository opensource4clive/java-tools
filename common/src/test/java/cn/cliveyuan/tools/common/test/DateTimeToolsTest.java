package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.DateTimeTools;
import cn.cliveyuan.tools.common.enums.DateTimeField;
import cn.cliveyuan.tools.common.enums.DateTimeFormat;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;


/**
 * @author clive
 * Created on 2018/07/23
 * @since 1.0
 */
public class DateTimeToolsTest {

    @Test
    public void test() throws ParseException {
        System.out.println("now: " + DateTimeTools.now());
        System.out.println("local now: " + DateTimeTools.nowLocalDateTime());
        System.out.println("set date: " + DateTimeTools.ofDate(2018, 10, 13));
        System.out.println("set time: " + DateTimeTools.ofTime(12, 19, 0));
        System.out.println("2009-01-01 is before now: " + DateTimeTools.beforeNow(DateTimeTools.ofDate(2009, 1, 1)));
        System.out.println("2099-01-01 is after now: " + DateTimeTools.afterNow(DateTimeTools.ofDate(2099, 1, 1)));

        System.out.println("now format: " + DateTimeTools.format(DateTimeFormat.DATE_TIME_1));

        System.out.println("now parse: " + DateTimeTools.parse("2008-09-01 19:00:09", DateTimeFormat.DATE_TIME_1));

        System.out.println("now plus 1 day is: " + DateTimeTools.plus(DateTimeField.DAY, 1));
        System.out.println("now plus -1 hours is: " + DateTimeTools.plus(DateTimeField.HOUR, -1));
    }

    @Test
    public void test2() {
        int ts = (int) (System.currentTimeMillis() / 1000);
        Assert.assertEquals(ts, DateTimeTools.nowSeconds());
        Date date = DateTimeTools.parseTimestamp(ts);
        Assert.assertEquals(ts, date.getTime() / 1000);

        String dateStr1 = "2021-02-09 17:30:06";
        Date date2 = DateTimeTools.parseStandardDate(dateStr1);
        Assert.assertEquals("2021-02-09 00:00:00", DateTimeTools.getStartTimeString(date2));
        Assert.assertEquals("2021-02-09 23:59:59", DateTimeTools.getEndTimeString(date2));

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.setTime(DateTimeTools.plusDay(1));
        Assert.assertEquals(day + 1, calendar.get(Calendar.DAY_OF_MONTH));
        calendar.setTime(DateTimeTools.plusDay(-1));
        Assert.assertEquals(day - 1, calendar.get(Calendar.DAY_OF_MONTH));

        LocalDateTime localDateTime = DateTimeTools.date2LocalDateTime(date2);
        Assert.assertEquals(date2.getTime() / 1000, localDateTime.atZone(ZoneId.systemDefault()).toEpochSecond());
        Date date3 = DateTimeTools.localDateTime2Date(localDateTime);
        Assert.assertEquals(date2, date3);

        String dateStr = DateTimeTools.formatStandardDate(date2);
        Assert.assertEquals(dateStr1, dateStr);
    }
}
