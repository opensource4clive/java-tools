package cn.cliveyuan.tools.common.test;

import cn.cliveyuan.tools.common.SystemTools;
import cn.cliveyuan.tools.common.enums.OSEnum;
import org.junit.Test;

/**
 * @author Clive Yuan
 * @date 2022/06/25
 */
public class SystemToolsTest {
    @Test
    public void getOs() {
        OSEnum os = SystemTools.getOs();
        System.out.println(os);
    }
}
