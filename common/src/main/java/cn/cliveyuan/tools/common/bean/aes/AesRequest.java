package cn.cliveyuan.tools.common.bean.aes;

import lombok.Builder;
import lombok.Data;

/**
 * AES 加解密请求
 *
 * @author Clive Yuan
 * @date 2021/09/02
 */
@Data
@Builder
public class AesRequest {
    /**
     * 模式
     */
    @Builder.Default
    private String transformation = "AES/ECB/PKCS5Padding";
    /**
     * 算法
     */
    @Builder.Default
    private String algorithm = "AES";
    /**
     * 密钥
     */
    private byte[] key;
    /**
     * 向量
     */
    private byte[] iv;
    /**
     * 数据
     */
    private byte[] data;
    /**
     * 是否打印错误日志
     */
    @Builder.Default
    private boolean printErrorLog = true;
    /**
     * 添加 provider, 需自行加载类似以下代码
     * <pre>
     *     static {
     *         Security.addProvider(new BouncyCastleProvider());
     *     }
     * </pre>
     */
    private String provider;
}
