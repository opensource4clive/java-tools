package cn.cliveyuan.tools.common.bean.csv;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

/**
 * @author Clive Yuan
 * @date 2020/06/22
 */
@Data
@Builder
public class CsvWriter<T> implements Serializable {
    /**
     * 抬头 (要与数据中的属性顺序和数量对应)
     */
    private String[] headers;
    /**
     * 数据
     */
    private Collection<T> data;
    /**
     * 导出的文件路径
     */
    private String pathname;
    /**
     * 文件名, 不指定则随机生成
     */
    private String fileName;
    /**
     * 编码格式: 默认UTF-8
     */
    @Builder.Default
    private String encoding = StandardCharsets.UTF_8.displayName();
}
