package cn.cliveyuan.tools.common.bean.csv;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

/**
 * @author Clive Yuan
 * @date 2020/06/22
 */
@Data
@Builder
public class CsvReader<T> implements Serializable {

    /**
     * csv文件绝对路径
     */
    private String filePath;
    /**
     * 跳过的行数
     */
    @Builder.Default
    private int skipRowNo = 1;
    /**
     * 编码格式: 默认UTF-8
     */
    @Builder.Default
    private String encoding = StandardCharsets.UTF_8.displayName();
    /**
     * 数据对应的类
     */
    private Class<T> clazz;
}
