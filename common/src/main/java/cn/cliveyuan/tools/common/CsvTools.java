package cn.cliveyuan.tools.common;

import cn.cliveyuan.tools.common.bean.csv.CsvReader;
import cn.cliveyuan.tools.common.bean.csv.CsvWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyDescriptor;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * @author clive
 * Created on 2018/08/01
 * @since 1.0
 */
public class CsvTools {

    private static final Logger logger = LoggerFactory.getLogger(CsvTools.class);

    private final static String CSV = "csv";
    private static final String EXTENSION = "." + CSV;

    /**
     * 读取CSV文件
     *
     * @param csvReader csvReader
     * @param <T>       对应的实体
     * @return 数据列表
     */
    public static <T> List<T> read(CsvReader<T> csvReader) {
        AssertTools.notNull(csvReader, "csvReader is required");
        AssertTools.notNull(csvReader.getFilePath(), "filePath is required");
        AssertTools.notNull(csvReader.getClazz(), "clazz is required");
        List<String[]> data = CsvTools.readRawCsv(csvReader.getFilePath(), csvReader.getEncoding());
        return TableUtils.dataToObject(csvReader.getSkipRowNo(), data, csvReader.getClazz());
    }

    /**
     * 写入CSV文件
     *
     * @param csvWriter csvWriter
     * @param <T>       对应的实体
     * @return CSV文件
     */
    public static <T> File write(CsvWriter<T> csvWriter) {
        AssertTools.notEmpty(csvWriter.getHeaders(), "headers can't be empty");
        AssertTools.notEmpty(csvWriter.getData(), "data can't be empty");
        AssertTools.notNull(csvWriter.getPathname(), "file path can't be empty");
        return doWrite(csvWriter);
    }

    /**
     * 读入原生csv文件，解析后返回
     *
     * @param absoluteFilePath 文件绝对路径
     */
    public static List<String[]> readRawCsv(String absoluteFilePath, String encoding) {
        AssertTools.notBlank(absoluteFilePath, "csv file path is empty");
        File file = new File(absoluteFilePath);
        AssertTools.isTrue(file.exists(), "csv file not exists");
        List<String[]> list = new ArrayList<>();
        try {
            String extension = FileTools.getExtension(absoluteFilePath);
            if (!CSV.equalsIgnoreCase(extension)) {
                throw new RuntimeException("not support this file: " + extension);
            }
            List<String> lines = FileTools.readFileToStringList(file, encoding);
            lines.forEach(line -> list.add(line.split(",")));
        } catch (Exception e) {
            logger.error("readCsv ", e);
            throw new RuntimeException(e);
        }
        return list;
    }

    //region private methods

    private static <T> File doWrite(CsvWriter<T> csvWriter) {
        String filePath = csvWriter.getPathname();
        String[] headers = csvWriter.getHeaders();
        Collection<T> data = csvWriter.getData();
        File file = new File(filePath);
        if (!file.exists()) {
            boolean result = file.mkdirs();
            if (!result) {
                throw new RuntimeException("Fail to execute mkdirs");
            }
        }
        String name = csvWriter.getFileName();
        if (StringTools.isBlank(name)) {
            name = UUID.randomUUID().toString();
        }
        String fileName = name + EXTENSION;
        if (!filePath.endsWith(File.separator)) {
            filePath = filePath + File.separator;
        }
        file = new File(filePath + fileName);
        try {
            StringBuilder content = new StringBuilder();
            // header
            for (String header : headers) {
                content.append(header).append(",");
            }
            content.deleteCharAt(content.length() - 1);
            content.append("\n");

            // data
            for (T t : data) {
                //反射
                Class<?> clazz = t.getClass();
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    PropertyDescriptor pd = new PropertyDescriptor(field.getName(), clazz);
                    Method getMethod = pd.getReadMethod();
                    if (getMethod != null) {
                        Object invoke = getMethod.invoke(t);
                        String value = StringTools.EMPTY;
                        if (invoke != null) {
                            if (invoke instanceof Date) {
                                value = TableUtils.SDF.format((Date) invoke);
                            } else {
                                value = invoke.toString();
                            }
                        }
                        content.append(value).append(",");
                    }
                }
                content.deleteCharAt(content.length() - 1);
                content.append("\n");
            }
            FileTools.writeStringToFile(file, content.toString(), csvWriter.getEncoding());
            return file;
        } catch (Exception e) {
            logger.error("excel generate exception", e);
            throw new RuntimeException(e);
        }
    }
    //endregion
}
