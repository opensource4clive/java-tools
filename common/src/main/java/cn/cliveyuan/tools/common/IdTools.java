package cn.cliveyuan.tools.common;

import cn.cliveyuan.tools.common.enums.DateTimeFormat;
import cn.cliveyuan.tools.common.inner.SnowflakeIdWorker;
import org.apache.commons.lang3.RandomUtils;

import java.util.UUID;

/**
 * ID生成工具
 *
 * @author clive
 * Created on 2018/07/27
 * @since 1.0
 */
public class IdTools {

    private IdTools() {
    }

    private static final SnowflakeIdWorker SNOWFLAKE_ID_WORKER = new SnowflakeIdWorker(0, 0);

    /**
     * 随机一个原生UUID
     */
    public static String randomUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * 随机去掉横杠的UUID
     */
    public static String randomShortUUID() {
        String uuid = randomUUID();
        // 去掉“-”符号
        return uuid.replaceAll("-", "");
    }

    /**
     * 生成雪花算法ID (单机版)
     * 长度: 18个字符
     */
    public static String genSnowflakeId() {
        return Long.toString(SNOWFLAKE_ID_WORKER.nextId());
    }

    /**
     * 生成雪花算法ID (分布式版)
     * 长度: 18个字符
     *
     * @param workerId     工作ID (0~31)
     * @param datacenterId 数据中心ID (0~31)
     */
    public static String genSnowflakeId(int workerId, int datacenterId) {
        return Long.toString(new SnowflakeIdWorker(workerId, datacenterId).nextId());
    }
}
