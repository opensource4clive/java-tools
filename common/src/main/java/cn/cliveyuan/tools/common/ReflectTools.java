package cn.cliveyuan.tools.common;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 反射工具类
 *
 * @author Clive Yuan
 * @date 2022/06/25
 * @since 4.0.4
 */
public class ReflectTools {

    /**
     * 初始化
     *
     * @param clazz 类
     * @param <T>   泛型
     */
    public static <T> T newInstance(Class<?> clazz) {
        try {
            return (T) clazz.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取所有声明字段,且非static修饰
     *
     * @param clazz 类对象
     */
    public static List<Field> getDeclaredFields(Class<?> clazz) {
        Field[] declaredFields = clazz.getDeclaredFields();
        return Arrays.stream(declaredFields).filter(x -> !Modifier.isStatic(x.getModifiers()))
                .collect(Collectors.toList());

    }

    public static List<Method> getDeclaredMethods(Class<?> clazz) {
        Method[] methods = clazz.getDeclaredMethods();
        return Arrays.stream(methods).collect(Collectors.toList());
    }

    public static Method getSetMethod(Class<?> clazz, String fieldName) {
        try {
            return Arrays.stream(clazz.getDeclaredMethods())
                    .filter(x -> x.getName().equals("set".concat(StringTools.capitalize(fieldName))))
                    .findFirst().orElseThrow(NoSuchMethodException::new);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    public static Method getGetMethod(Class<?> clazz, String fieldName) {
        try {
            return clazz.getDeclaredMethod("get".concat(StringTools.capitalize(fieldName)));
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    public static Object invokeMethod(Method method, Object obj, Object... args) {
        try {
            method.setAccessible(true);
            return method.invoke(obj, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
