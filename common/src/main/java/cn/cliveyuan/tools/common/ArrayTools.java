package cn.cliveyuan.tools.common;

import org.apache.commons.lang3.ArrayUtils;

/**
 * 数组工具
 *
 * @author Clive Yuan
 * @date 2020/06/16
 */
public class ArrayTools extends ArrayUtils {

    private ArrayTools() {
    }
}
