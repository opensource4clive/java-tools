package cn.cliveyuan.tools.common;

import cn.cliveyuan.tools.common.annotation.XmlField;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

/**
 * XML工具
 *
 * @author Clive Yuan
 * @date 2022/06/25
 * @since 4.0.4
 */
@Slf4j
public class XmlTools {

    private static final String ROOT_ELEMENT = "<xml>%n%s%n</xml>";
    private static final String ELEMENT_TPL = "<%s>%s</%s>";

    /**
     * 解析xml为对象
     *
     * @param xmlContent xml内容
     * @param clazz      类
     */
    public static <T> T parseObject(String xmlContent, Class<T> clazz) {
        AssertTools.notBlank(xmlContent, "xmlContent is required");
        AssertTools.notNull(clazz, "class is required");
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(xmlContent.getBytes())) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(byteArrayInputStream);
            Element rootElement = document.getDocumentElement();
            Object object = ReflectTools.newInstance(clazz);
            List<Field> declaredFields = ReflectTools.getDeclaredFields(clazz);
            declaredFields.forEach(field -> {
                String name = field.getName();
                XmlField xmlField = field.getAnnotation(XmlField.class);
                if (Objects.nonNull(xmlField)) {
                    name = xmlField.value();
                }
                NodeList nodeList = rootElement.getElementsByTagName(name);
                if (nodeList.getLength() > 0) {
                    String value = nodeList.item(0).getTextContent();
                    Method setMethod = ReflectTools.getSetMethod(clazz, field.getName());
                    ReflectTools.invokeMethod(setMethod, object, value);
                }
            });
            return (T) object;
        } catch (Exception e) {
            log.error("XmlTools.parseObject", e);
        }

        return null;
    }

    /**
     * 对象转XML
     *
     * @param object 对象
     */
    public static String toXmlString(Object object) {
        AssertTools.notNull(object, "object is required");
        StringBuilder sb = new StringBuilder();
        Class<?> clazz = object.getClass();
        List<Field> declaredFields = ReflectTools.getDeclaredFields(clazz);
        declaredFields.forEach(field -> {
            String name = field.getName();
            XmlField xmlField = field.getAnnotation(XmlField.class);
            if (Objects.nonNull(xmlField)) {
                name = xmlField.value();
            }
            Method getMethod = ReflectTools.getGetMethod(clazz, field.getName());
            Object value = ReflectTools.invokeMethod(getMethod, object);
            String val = Objects.nonNull(value) ? value.toString() : "";
            // <return_code><![CDATA[FAIL]]></return_code>
            sb.append(String.format(ELEMENT_TPL, name, val, name)).append("\n");
        });
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return String.format(ROOT_ELEMENT, sb);
    }
}
