package cn.cliveyuan.tools.common;

import cn.cliveyuan.tools.common.enums.DateTimeField;
import cn.cliveyuan.tools.common.enums.DateTimeFormat;
import lombok.SneakyThrows;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期时间工具
 *
 * @author clive
 */
public class DateTimeTools {

    private DateTimeTools() {
    }

    /**
     * 当前时间
     */
    public static Date now() {
        return new Date();
    }

    /**
     * 当前本地时间
     */
    public static LocalDateTime nowLocalDateTime() {
        return LocalDateTime.now();
    }

    /**
     * 通过日期生成
     *
     * @param year       年
     * @param month      月
     * @param dayOfMonth 日
     */
    public static Date ofDate(int year, int month, int dayOfMonth) {
        return localDateTime2Date(LocalDateTime.of(year, month, dayOfMonth, 0, 0, 0));
    }

    /**
     * 通过时间生成
     *
     * @param hour   小时
     * @param minute 分钟
     * @param second 秒
     */
    public static Date ofTime(int hour, int minute, int second) {
        LocalDateTime localDateTime = nowLocalDateTime();
        return localDateTime2Date(LocalDateTime.of(localDateTime.getYear(),
                localDateTime.getMonth(), localDateTime.getDayOfMonth(),
                hour, minute, second));
    }

    /**
     * 之前
     *
     * @param date1 date1
     * @param date2 date2
     */
    public static boolean before(Date date1, Date date2) {
        AssertTools.notNull(date1, "before: date1 can't be null");
        AssertTools.notNull(date2, "before: date2 can't be null");
        return date1.before(date2);
    }

    /**
     * 之后
     *
     * @param date1 date1
     * @param date2 date2
     */
    public static boolean after(Date date1, Date date2) {
        AssertTools.notNull(date1, "after: date1 can't be null");
        AssertTools.notNull(date2, "after: date2 can't be null");
        return date1.after(date2);
    }

    /**
     * 现在之前
     *
     * @param date date
     */
    public static boolean beforeNow(Date date) {
        AssertTools.notNull(date, "beforeNow: date can't be null");
        return before(date, now());
    }

    /**
     * 现在之后
     *
     * @param date date
     */
    public static boolean afterNow(Date date) {
        AssertTools.notNull(date, "afterNow: date can't be null");
        return after(date, now());
    }

    /**
     * 当前时间格式化
     *
     * @param dateTimeFormat dateTimeFormat
     */
    public static String format(DateTimeFormat dateTimeFormat) {
        return format(now(), dateTimeFormat);
    }

    /**
     * 标准日期格式化
     * yyyy-MM-dd HH:mm:ss
     *
     * @param date date
     * @return
     */
    public static String formatStandardDate(Date date) {
        return format(date, DateTimeFormat.DATE_TIME_1);
    }

    /**
     * 日期格式化
     *
     * @param date   日期
     * @param format 格式
     */
    public static String format(Date date, DateTimeFormat format) {
        return format(date, format.getFormat());
    }

    /**
     * 日期格式化
     *
     * @param date   日期
     * @param format 格式
     */
    public static String format(Date date, String format) {
        AssertTools.notNull(date, "format: date can't be null");
        AssertTools.notEmpty(format, "format: format can't be empty");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }


    /**
     * 日期解析
     *
     * @param dateStr 日期字符串
     * @param format  格式
     */
    @SneakyThrows
    public static Date parse(String dateStr, String format) {
        AssertTools.notEmpty(dateStr, "dateStr can't be empty");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.parse(dateStr);
    }

    /**
     * 日期解析
     *
     * @param dateStr 日期字符串
     * @param format  格式
     */
    public static Date parse(String dateStr, DateTimeFormat format) {
        AssertTools.notNull(format, "parse: format is required");
        return parse(dateStr, format.getFormat());
    }

    /**
     * 标准日期解析
     * yyyy-MM-dd HH:mm:ss
     *
     * @param dateStr 日期字符串
     */
    public static Date parseStandardDate(String dateStr) {
        return parse(dateStr, DateTimeFormat.DATE_TIME_1);
    }

    /**
     * 日期时间调整
     *
     * @param field 字段
     * @param date  日期 (基于此时间调整)
     * @param value 值 (正值增加,负值减少)
     */
    public static Date plus(DateTimeField field, Date date, long value) {
        AssertTools.notNull(field, "plus: field is required");
        LocalDateTime localDateTime = date2LocalDateTime(date);
        switch (field) {
            case YEAR:
                localDateTime = localDateTime.plusYears(value);
                break;
            case MONTH:
                localDateTime = localDateTime.plusMonths(value);
                break;
            case WEEK:
                localDateTime = localDateTime.plusWeeks(value);
                break;
            case DAY:
                localDateTime = localDateTime.plusDays(value);
                break;
            case HOUR:
                localDateTime = localDateTime.plusHours(value);
                break;
            case MINUTE:
                localDateTime = localDateTime.plusMinutes(value);
                break;
            case SECOND:
                localDateTime = localDateTime.plusSeconds(value);
                break;
        }
        return localDateTime2Date(localDateTime);
    }

    /**
     * 日期时间调整(基于当前时间)
     *
     * @param field 字段
     * @param value (正值增加,负值减少)
     */
    public static Date plus(DateTimeField field, long value) {
        return plus(field, now(), value);
    }

    /**
     * 基于今日调整天
     *
     * @param value 天数
     */
    public static Date plusDay(int value) {
        return plus(DateTimeField.DAY, value);
    }

    /**
     * 当前时间的毫秒
     */
    public static long nowMillis() {
        return System.currentTimeMillis();
    }

    /**
     * 当前时间的秒
     */
    public static int nowSeconds() {
        return (int) Instant.now().getEpochSecond();
    }

    /**
     * Unix时间戳字符串
     */
    public static String tsString() {
        return Integer.toString(nowSeconds());
    }

    /**
     * 计算耗时 单位 毫秒
     *
     * @param startTime 开始时间
     */
    public static long costTime(long startTime) {
        return nowMillis() - startTime;
    }

    /**
     * 计算耗时 单位:秒
     *
     * @param startTime 开始时间: 毫秒
     */
    public static int costTimeSecond(long startTime) {
        return (int) (costTime(startTime) / 1000L);
    }

    /**
     * 今日日期
     */
    public static Date getTodayDate() {
        return DateTimeTools.parse(getTodayDateString(), DateTimeFormat.DATE_1);
    }

    /**
     * 今日日期字符串
     */
    public static String getTodayDateString() {
        return DateTimeTools.format(DateTimeFormat.DATE_1);
    }

    /**
     * 获取开始时间 00:00:00
     *
     * @param date 日期
     */
    public static Date getStartTime(Date date) {
        AssertTools.notNull(date, "getStartTime: date is required");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTime();
    }

    /**
     * 获取开始时间 00:00:00
     *
     * @param date 日期
     */
    public static String getStartTimeString(Date date) {
        return formatStandardDate(getStartTime(date));
    }

    /**
     * 获取结束时间 23:59:59
     *
     * @param date 日期
     */
    public static Date getEndTime(Date date) {
        AssertTools.notNull(date, "getEndTime: date is required");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    /**
     * 获取结束时间 23:59:59
     *
     * @param date 日期
     */
    public static String getEndTimeString(Date date) {
        return formatStandardDate(getEndTime(date));
    }

    /**
     * 解析时间戳
     *
     * @param timestamp 时间戳
     */
    public static Date parseTimestamp(Integer timestamp) {
        AssertTools.notNull(timestamp, "parseTimestamp: timestamp is required");
        return new Date(timestamp * 1000L);
    }

    /**
     * 日期转本地日期
     *
     * @param date 日期
     */
    public static LocalDateTime date2LocalDateTime(Date date) {
        AssertTools.notNull(date, "date2LocalDateTime: date is required");
        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    /**
     * 本地日期转日期
     *
     * @param localDateTime 本地日期
     */
    public static Date localDateTime2Date(LocalDateTime localDateTime) {
        AssertTools.notNull(localDateTime, "localDateTime2Date: localDateTime is required");
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
