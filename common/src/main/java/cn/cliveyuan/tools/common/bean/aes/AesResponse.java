package cn.cliveyuan.tools.common.bean.aes;

import lombok.Builder;
import lombok.Data;

/**
 * Aes加解密响应
 *
 * @author Clive Yuan
 * @date 2021/09/02
 */
@Data
@Builder
public class AesResponse {

    /**
     * 是否成功
     */
    private boolean success;
    /**
     * 错误消息
     */
    private String msg;
    /**
     * 数据
     */
    private byte[] data;
}
