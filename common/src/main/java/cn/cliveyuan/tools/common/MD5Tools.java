package cn.cliveyuan.tools.common;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * MD5工具
 *
 * @author clive
 * Created on 2018/07/24
 * @since 1.0
 */
public class MD5Tools {

    private MD5Tools() {
    }

    /**
     * md5加密
     *
     * @param text 文本
     */
    public static String md5(String text) {
        return DigestUtils.md5Hex(text);
    }

    /**
     * md5 加盐加密
     *
     * @param text 文本
     * @param salt 盐 (密钥)
     */
    public static String md5(String text, String salt) {
        return md5(text + salt);
    }
}
