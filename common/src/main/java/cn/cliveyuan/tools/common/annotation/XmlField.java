package cn.cliveyuan.tools.common.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * XML字段注解
 *
 * @author Clive Yuan
 * @date 2022/06/25
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface XmlField {

    String value() default "";

    boolean ignore() default false;
}
