package cn.cliveyuan.tools.common.exception;

/**
 * 邮件异常
 *
 * @author clive
 * Created on 2018/07/23
 * @since 1.0
 */
public class EmailException extends BaseException {


    public EmailException(int code, String message) {
        super(code, message);
    }

    public static EmailException connectFail(String message) {
        return new EmailException(1000, message);
    }

    public static EmailException authFail() {
        return new EmailException(1001, "account or password is wrong of email sender");
    }

    public static EmailException invalidAddress() {
        return new EmailException(1002, "Invalid Addresses");
    }

    public static EmailException unKnowError() {
        return new EmailException(1003, "UnKnow Error");
    }
}
