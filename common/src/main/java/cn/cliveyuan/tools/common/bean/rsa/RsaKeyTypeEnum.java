package cn.cliveyuan.tools.common.bean.rsa;

/**
 * RSA密钥类型
 *
 * @author Clive Yuan
 * @date 2021/09/03
 */
public enum RsaKeyTypeEnum {
    PRIVATE_KEY,
    PUBLIC_KEY
}
