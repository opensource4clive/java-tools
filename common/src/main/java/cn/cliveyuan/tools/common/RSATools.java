package cn.cliveyuan.tools.common;

import cn.cliveyuan.tools.common.bean.rsa.GenerateKeyPairReq;
import cn.cliveyuan.tools.common.bean.rsa.RSAKeyPair;
import cn.cliveyuan.tools.common.bean.rsa.RsaKeyTypeEnum;
import cn.cliveyuan.tools.common.bean.rsa.RsaRequest;
import cn.cliveyuan.tools.common.bean.rsa.RsaResponse;
import cn.cliveyuan.tools.common.bean.rsa.RsaSignRequest;
import cn.cliveyuan.tools.common.bean.rsa.RsaSignResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Objects;

/**
 * RSA 工具
 * <ol>
 *     <li>密钥生成</li>
 *     <li>加密/解密</li>
 *     <li>签名/验签</li>
 * </ol>
 */
@Slf4j
public class RSATools {

    /**
     * 生成RSA公钥私钥 (RSA,2048)
     */
    public static RSAKeyPair generateKeyPair() {
        return generateKeyPair(GenerateKeyPairReq.builder().build());
    }

    /**
     * 生成RSA公钥私钥
     *
     * @param generateKeyPairReq generateKeyPairReq
     * @return
     */
    public static RSAKeyPair generateKeyPair(GenerateKeyPairReq generateKeyPairReq) {
        try {
            AssertTools.notNull(generateKeyPairReq, "param is required");
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(generateKeyPairReq.getKeyAlgorithm());
            keyPairGenerator.initialize(generateKeyPairReq.getKeySize());
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            PublicKey pubKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();
            return RSAKeyPair.builder()
                    .publicKey(Base64.encodeBase64String(pubKey.getEncoded()))
                    .privateKey(Base64.encodeBase64String(privateKey.getEncoded()))
                    .build();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 验签
     *
     * @param data            需要验证签名的数据字符串
     * @param sign            签名字符串(长度：1024-->128 2048-->256)
     * @param publicKeyString RSA公钥
     * @return boolean true : 验证签名成功 false: 验证签名失败
     */
    public static boolean verifySign(String data, String sign, String publicKeyString) {
        RsaSignResponse rsaSignResponse = doSign(RsaSignRequest.builder()
                .data(data.getBytes())
                .key(Base64.decodeBase64(publicKeyString))
                .isVerifySign(true)
                .sign(Base64.decodeBase64(sign))
                .build());
        if (rsaSignResponse.isSuccess()) {
            return rsaSignResponse.isVerifySignResult();
        }
        return false;
    }

    /**
     * 用私钥对信息生成数字签名
     *
     * @param data             数据
     * @param privateKeyString 私钥
     * @return
     */
    public static String sign(String data, String privateKeyString) {
        RsaSignResponse rsaSignResponse = doSign(RsaSignRequest.builder()
                .data(data.getBytes())
                .key(Base64.decodeBase64(privateKeyString))
                .build());
        if (rsaSignResponse.isSuccess()) {
            return Base64.encodeBase64String(rsaSignResponse.getSign());
        }
        return null;
    }

    /**
     * 签名/验签
     *
     * @param request 参数
     * @return
     */
    public static RsaSignResponse doSign(RsaSignRequest request) {
        AssertTools.notNull(request, "request is required");
        AssertTools.notNull(request.getKey(), "key is required");
        AssertTools.notNull(request.getData(), "data is required");

        try {
            Signature signature = Signature.getInstance(request.getSignatureAlgorithm());
            KeyFactory keyFactory = KeyFactory.getInstance(request.getKeyAlgorithm());
            if (request.isVerifySign()) {
                // 验签
                PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(request.getKey()));
                signature.initVerify(publicKey);
                signature.update(request.getData());
                return RsaSignResponse.builder().success(true)
                        .verifySignResult(signature.verify(request.getSign()))
                        .build();
            } else {
                // 签名
                PrivateKey privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(request.getKey()));
                signature.initSign(privateKey);
                signature.update(request.getData());
                return RsaSignResponse.builder().success(true)
                        .sign(signature.sign())
                        .build();
            }
        } catch (Exception e) {
            if (request.isPrintErrorLog()) {
                log.error("doSign", e);
            }
            return RsaSignResponse.builder().success(false)
                    .msg(e.getMessage())
                    .build();
        }
    }

    /**
     * 公钥加密
     *
     * @param request 参数
     * @return
     */
    public static RsaResponse encrypt(RsaRequest request) {
        request.setRsaKeyTypeEnum(RsaKeyTypeEnum.PUBLIC_KEY);
        return cipher(request, Cipher.ENCRYPT_MODE);
    }

    /**
     * 私钥解密
     *
     * @param request 参数
     * @return
     */
    public static RsaResponse decrypt(RsaRequest request) {
        request.setRsaKeyTypeEnum(RsaKeyTypeEnum.PRIVATE_KEY);
        return cipher(request, Cipher.DECRYPT_MODE);
    }

    /**
     * 计算加解密
     *
     * @param request 参数
     * @param mode    模式
     * @return
     */
    public static RsaResponse cipher(RsaRequest request, int mode) {
        validate(request);
        try {
            String algorithm = request.getKeyAlgorithm();
            String cipherAlgorithm = request.getCipherAlgorithm();
            KeyFactory keyFactory;
            Cipher cipher;
            if (Objects.nonNull(request.getProvider())) {
                keyFactory = KeyFactory.getInstance(algorithm, request.getProvider());
                cipher = Cipher.getInstance(cipherAlgorithm, request.getProvider());
            } else {
                keyFactory = KeyFactory.getInstance(algorithm);
                cipher = Cipher.getInstance(cipherAlgorithm);
            }
            Key key = getKey(request, keyFactory);
            cipher.init(mode, key);
            byte[] data = cipher.doFinal(request.getData());
            return RsaResponse.builder().success(true).data(data).build();
        } catch (Exception e) {
            if (request.isPrintErrorLog()) {
                log.error("decrypt Exception", e);
            }
            return RsaResponse.builder().success(false).msg(e.getMessage()).build();
        }
    }

    /**
     * 公钥加密
     *
     * @param data      数据
     * @param base64Key 公钥
     * @return
     */
    public static String encrypt(String data, String base64Key) {
        return encryptBase64(data, base64Key);
    }

    /**
     * 私钥解密
     *
     * @param data      数据
     * @param base64Key 私钥
     * @return
     */
    public static String decrypt(String data, String base64Key) {
        return decryptBase64(data, base64Key);
    }

    public static String encryptBase64(String data, String base64Key) {
        RsaResponse rsaResponse = encrypt(RsaRequest.builder()
                .data(data.getBytes())
                .key(Base64.decodeBase64(base64Key))
                .build());
        if (rsaResponse.isSuccess()) {
            return Base64.encodeBase64String(rsaResponse.getData());
        }
        return null;
    }

    public static String decryptBase64(String data, String base64Key) {
        RsaResponse rsaResponse = decrypt(RsaRequest.builder()
                .data(Base64.decodeBase64(data))
                .key(Base64.decodeBase64(base64Key))
                .build());
        if (rsaResponse.isSuccess()) {
            return new String(rsaResponse.getData(), StandardCharsets.UTF_8);
        }
        return null;
    }

    private static Key getKey(RsaRequest request, KeyFactory keyFactory) throws InvalidKeySpecException {
        switch (request.getRsaKeyTypeEnum()) {
            case PUBLIC_KEY:
                return keyFactory.generatePublic(new X509EncodedKeySpec(request.getKey()));
            case PRIVATE_KEY:
                return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(request.getKey()));
            default:
                throw new RuntimeException("Not support key type: " + request.getRsaKeyTypeEnum());
        }
    }

    private static void validate(RsaRequest request) {
        AssertTools.notNull(request);
        AssertTools.notNull(request.getKey(), "key is required");
        AssertTools.notNull(request.getData(), "data is required");
        AssertTools.notNull(request.getRsaKeyTypeEnum(), "rsaKeyType is required");
    }
}
