package cn.cliveyuan.tools.common;

/**
 * 线程工具
 *
 * @author clive
 * Created on 2018/07/27
 * @since 1.0
 */
public class ThreadTools {

    private ThreadTools() {
    }

    /**
     * 异步执行
     * 适合调用不是很频繁的任务
     * 频繁任务请使用线程池
     *
     * @param runnable 执行线程
     */
    public static void async(Runnable runnable) {
        new Thread(runnable).start();
    }

    /**
     * 线程睡眠
     *
     * @param seconds 秒
     */
    public static void sleep(int seconds) {
        sleep((long) seconds * 1000L);
    }

    /**
     * 线程睡眠
     *
     * @param millis 毫秒
     */
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
