package cn.cliveyuan.tools.common.enums;

/**
 * 日期时间字段常量
 *
 * @author clive
 * Created on 2018/07/23
 * @since 1.0
 */
public enum DateTimeField {
    YEAR,
    MONTH,
    WEEK,
    DAY,
    HOUR,
    MINUTE,
    SECOND
}
