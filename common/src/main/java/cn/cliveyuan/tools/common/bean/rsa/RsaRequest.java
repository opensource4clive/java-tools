package cn.cliveyuan.tools.common.bean.rsa;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Clive on 2020/02/22.
 * Updated by Clive on 2021/09/03.
 */
@Data
@Builder
public class RsaRequest implements Serializable {

    /**
     * 数据
     */
    private byte[] data;

    /**
     * RSA公钥/私钥
     */
    private byte[] key;

    /**
     * 密钥类型
     */
    private RsaKeyTypeEnum rsaKeyTypeEnum;

    /**
     * 密钥算法, 默认 RSA
     */
    @Builder.Default
    private String keyAlgorithm = "RSA";

    /**
     * 加密算法, 默认 RSA
     * <p>
     * 例如:
     * <ul>
     *     <li>RSA</li>
     *     <li>RSA/None/PKCS1Padding</li>
     *     <li>RSA/ECB/PKCS1Padding</li>
     * </ul>
     * </p>
     */
    @Builder.Default
    private String cipherAlgorithm = "RSA";

    /**
     * 添加BouncyCastle, 需自行加载以下代码
     * <pre>
     *     static {
     *         Security.addProvider(new BouncyCastleProvider());
     *     }
     * </pre>
     */
    private String provider;

    /**
     * 是否打印错误日志
     */
    @Builder.Default
    private boolean printErrorLog = true;

}
