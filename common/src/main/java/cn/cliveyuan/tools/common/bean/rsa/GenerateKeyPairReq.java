package cn.cliveyuan.tools.common.bean.rsa;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Clive on 2020/02/22.
 */
@Data
@Builder
public class GenerateKeyPairReq implements Serializable {
    /**
     * 密钥算法, 默认 RSA
     */
    @Builder.Default
    private String keyAlgorithm = "RSA";
    /**
     * 密钥长度, 默认 2048
     */
    @Builder.Default
    private Integer keySize = 2048;
}
