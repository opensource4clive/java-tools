package cn.cliveyuan.tools.common.enums;

/**
 * @author Clive Yuan
 * @date 2022/06/25
 */
public enum OSEnum {
    WINDOWS,
    MAC,
    LINUX,
    OTHER,
    UNKNOWN
}
