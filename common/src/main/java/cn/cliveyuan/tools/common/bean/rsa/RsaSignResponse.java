package cn.cliveyuan.tools.common.bean.rsa;

import lombok.Builder;
import lombok.Data;

/**
 * @author Clive Yuan
 * @date 2021/09/03
 */
@Data
@Builder
public class RsaSignResponse {

    /**
     * 是否成功
     */
    private boolean success;
    /**
     * 错误消息
     */
    private String msg;
    /**
     * 签名
     */
    private byte[] sign;
    /**
     * 签名验证结果
     */
    private boolean verifySignResult;
}
