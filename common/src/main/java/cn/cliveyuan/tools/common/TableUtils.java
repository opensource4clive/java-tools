package cn.cliveyuan.tools.common;

import lombok.extern.slf4j.Slf4j;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Clive on 2019/10/28.
 */
@Slf4j
public class TableUtils {

    public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static <T> List<T> dataToObject(int skipRowNo, List<String[]> data, Class<T> clazz) {
        List<T> list = new ArrayList<>();
        try {
            if (data == null || data.isEmpty()) return new ArrayList<>();
            Field[] fields = clazz.getDeclaredFields();
            if (skipRowNo > 0 && skipRowNo <= data.size()) {
                data = data.subList(skipRowNo, data.size());
            }
            for (String[] dataArray : data) {
                T t = clazz.newInstance();
                for (int i = 0; i < fields.length; i++) {
                    Field field = fields[i];
                    PropertyDescriptor pd = new PropertyDescriptor(field.getName(), clazz);
                    Method getMethod = pd.getWriteMethod();
                    if (getMethod != null) {
                        String strVal = StringTools.EMPTY;
                        if (i < dataArray.length) {
                            strVal = dataArray[i];
                        }
                        if (StringTools.isBlank(strVal)) continue;
                        Object value = strVal.trim();
                        Class<?>[] parameterTypes = getMethod.getParameterTypes();
                        Class<?> parameterType = parameterTypes[0];
                        if (Boolean.class.equals(parameterType) || boolean.class.equals(parameterType)) {
                            value = Boolean.valueOf(strVal.trim());
                        } else if (Integer.class.equals(parameterType) || int.class.equals(parameterType)) {
                            value = Integer.valueOf(strVal.trim());
                        } else if (Long.class.equals(parameterType) || long.class.equals(parameterType)) {
                            value = Long.valueOf(strVal.trim());
                        } else if (Double.class.equals(parameterType) || double.class.equals(parameterType)) {
                            value = Double.valueOf(strVal.trim());
                        } else if (Float.class.equals(parameterType) || float.class.equals(parameterType)) {
                            value = Float.valueOf(strVal.trim());
                        } else if (BigDecimal.class.equals(parameterType)) {
                            value = new BigDecimal(strVal.trim());
                        } else if (Date.class.equals(parameterType)) {
                            value = SDF.parse(strVal.trim());
                        } else if (String.class.equals(parameterType)) {
                            value = strVal.trim();
                        }
                        getMethod.invoke(t, value);
                    }
                }
                list.add(t);
            }
        } catch (Exception e) {
            log.error("dataToObject Exception", e);
        }
        return list;
    }
}
