package cn.cliveyuan.tools.common;

import cn.cliveyuan.tools.common.bean.aes.AesRequest;
import cn.cliveyuan.tools.common.bean.aes.AesResponse;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Objects;

/**
 * AES加密工具
 * <p>默认模式: AES/ECB/PKCS5Padding </p>
 *
 * @author Clive Yuan
 * Created on 2018-07-24
 * Updated on 2021-09-02
 */
public class AESTools {

    private static final Logger logger = LoggerFactory.getLogger(AESTools.class);

    private AESTools() {
    }

    /**
     * 加密 (Base64)
     *
     * @param key       密钥
     * @param plaintext 明文数据
     * @return base64密文数据
     */
    public static String encrypt(String key, String plaintext) {
        return encryptBase64(key, plaintext);
    }

    /**
     * 加密 (Base64)
     *
     * @param key       密钥
     * @param plaintext 明文数据
     * @return base64密文数据
     */
    public static String encryptBase64(String key, String plaintext) {
        AssertTools.notNull(key, "key is required");
        AssertTools.notNull(plaintext, "plaintext is required");
        AesResponse aesResponse = encrypt(AesRequest.builder()
                .key(key.getBytes())
                .data(plaintext.getBytes())
                .build());
        if (aesResponse.isSuccess()) {
            return Base64.encodeBase64String(aesResponse.getData());
        }
        return null;
    }

    /**
     * 解密 (Base64)
     *
     * @param key              密钥
     * @param base64CipherText 密文数据 (base64格式)
     * @return 明文数据
     */
    public static String decrypt(String key, String base64CipherText) {
        return decryptBase64(key, base64CipherText);
    }

    /**
     * 解密 (Base64)
     *
     * @param key              密钥
     * @param base64CipherText 密文数据 (base64格式)
     * @return 明文数据
     */
    public static String decryptBase64(String key, String base64CipherText) {
        AssertTools.notNull(key, "key is required");
        AssertTools.notNull(base64CipherText, "base64CipherText is required");
        AesResponse aesResponse = decrypt(AesRequest.builder()
                .key(key.getBytes())
                .data(Base64.decodeBase64(base64CipherText))
                .build());
        if (aesResponse.isSuccess()) {
            return new String(aesResponse.getData(), StandardCharsets.UTF_8);
        }
        return null;
    }

    /**
     * 加密 (Hex)
     *
     * @param key       密钥
     * @param plaintext 明文数据
     * @return hex密文数据
     */
    public static String encryptHex(String key, String plaintext) {
        AssertTools.notNull(key, "key is required");
        AssertTools.notNull(plaintext, "plaintext is required");
        AesResponse aesResponse = encrypt(AesRequest.builder()
                .key(key.getBytes())
                .data(plaintext.getBytes())
                .build());
        if (aesResponse.isSuccess()) {
            return Hex.encodeHexString(aesResponse.getData());
        }
        return null;
    }

    /**
     * 解密 (Hex)
     *
     * @param key           密钥
     * @param hexCipherText 密文数据 (hex格式)
     * @return 明文数据
     */
    @SneakyThrows
    public static String decryptHex(String key, String hexCipherText) {
        AssertTools.notNull(key, "key is required");
        AssertTools.notNull(hexCipherText, "hexCipherText is required");
        AesResponse aesResponse = decrypt(AesRequest.builder()
                .key(key.getBytes())
                .data(Hex.decodeHex(hexCipherText))
                .build());
        if (aesResponse.isSuccess()) {
            return new String(aesResponse.getData(), StandardCharsets.UTF_8);
        }
        return null;
    }

    public static AesResponse encrypt(AesRequest request) {
        return cipher(request, Cipher.ENCRYPT_MODE);
    }

    public static AesResponse decrypt(AesRequest request) {
        return cipher(request, Cipher.DECRYPT_MODE);
    }

    private static AesResponse cipher(AesRequest request, int mode) {

        validate(request);

        try {
            byte[] keyBytes = request.getKey();
            byte[] ivBytes = request.getIv();
            byte[] dataBytes = request.getData();
            Key key = generateKey(keyBytes, request.getAlgorithm());
            Cipher cipher;
            if (StringTools.isNotBlank(request.getProvider())) {
                cipher = Cipher.getInstance(request.getTransformation(), request.getProvider());
            } else {
                cipher = Cipher.getInstance(request.getTransformation());
            }
            if (Objects.nonNull(ivBytes)) {
                cipher.init(mode, key, generateIv(ivBytes, request.getAlgorithm()));
            } else {
                cipher.init(mode, key);
            }
            byte[] data = cipher.doFinal(dataBytes);
            return AesResponse.builder()
                    .success(true)
                    .data(data)
                    .build();
        } catch (Exception e) {
            if (request.isPrintErrorLog()) {
                logger.error("cipher Exception", e);
            }
            return AesResponse.builder()
                    .success(false)
                    .msg(e.getMessage())
                    .build();
        }
    }

    private static AlgorithmParameters generateIv(byte[] iv, String algorithm) throws NoSuchAlgorithmException, InvalidParameterSpecException {
        AlgorithmParameters params = AlgorithmParameters.getInstance(algorithm);
        params.init(new IvParameterSpec(iv));
        return params;
    }


    private static Key generateKey(byte[] key, String algorithm) {
        return new SecretKeySpec(key, algorithm);
    }

    private static void validate(AesRequest request) {
        AssertTools.notNull(request, "request is required");
        AssertTools.notNull(request.getKey(), "key is required");
        AssertTools.notNull(request.getData(), "data is required");
    }
}
