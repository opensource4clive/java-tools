package cn.cliveyuan.tools.common;

import cn.cliveyuan.tools.common.bean.EmailParam;
import cn.cliveyuan.tools.common.exception.EmailException;
import com.sun.mail.util.MailConnectException;
import com.sun.net.ssl.internal.ssl.Provider;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.Security;
import java.util.Objects;
import java.util.Properties;

/**
 * 邮件发送工具
 * <p>
 * Created by clive at 2018/07/23.
 * Rebuilt by clive at 2018/11/26.
 * Rebuilt by clive at 2021/06/09.
 *
 * @since 4.0.0
 */
public class EmailTools {

    private static final Logger logger = LoggerFactory.getLogger(EmailTools.class);
    private static final String CHARSET_UTF_8 = "text/html;charset=utf-8";

    private EmailTools() {
    }


    /**
     * 发送邮件
     *
     * @param param 参数
     */
    public static void send(EmailParam param) {

        //  验证
        ValidateTools.assertValidateEntity(param);

        //  判断发送类型
        if (param.isSSL()) {
            ssl(param);
        } else {
            normal(param);
        }
    }


    // region private methods

    private static void normal(EmailParam param) {
        try {
            Properties props = new Properties();
            // 发信的主机
            props.put("mail.smtp.host", param.getHost());
            props.put("mail.smtp.auth", "true");
            if (Objects.nonNull(param.getPort())) {
                props.setProperty("mail.smtp.port", param.getPort().toString());
            }
            Session session = Session.getInstance(props);
            // 调试开关
            session.setDebug(false);
            MimeMessage message = new MimeMessage(session);

            // 给消息对象设置发件人/收件人/主题/发信时间
            InternetAddress from = new InternetAddress(param.getAccount());
            message.setFrom(from);
            config(message, param);
            message.saveChanges();
            try (Transport transport = session.getTransport("smtp")) {
                transport.connect(param.getHost(), param.getAccount(), param.getPassword());
                transport.sendMessage(message, message.getAllRecipients());
            }
        } catch (MailConnectException e) {
            throw EmailException.connectFail(e.getMessage());
        } catch (AuthenticationFailedException e) {
            throw EmailException.authFail();
        } catch (SendFailedException e) {
            throw EmailException.invalidAddress();
        } catch (Exception e) {
            logger.error("send Exception", e);
            throw EmailException.unKnowError();
        }
    }

    private static void ssl(EmailParam param) {
        try {
            // 设置SSL连接、邮件环境
            Security.addProvider(new Provider());
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", param.getHost());
            String port = "465";
            if (Objects.nonNull(param.getPort())) {
                port = Integer.toString(param.getPort());
            }
            props.setProperty("mail.smtp.port", port);
            props.setProperty("mail.smtp.socketFactory.port", port);
            props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.setProperty("mail.smtp.socketFactory.fallback", "false");
            props.setProperty("mail.smtp.auth", "true");
            props.setProperty("mail.smtp.ssl.enable", "true");
            // 建立邮件会话
            Session session = Session.getDefaultInstance(props, new Authenticator() {
                // 身份认证
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(param.getAccount(), param.getPassword());
                }
            });
            session.setDebug(param.isDebug());
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(param.getAccount()));
            config(message, param);
            message.saveChanges();
            Transport.send(message);
        } catch (MailConnectException e) {
            throw EmailException.connectFail(e.getMessage());
        } catch (AuthenticationFailedException e) {
            throw EmailException.authFail();
        } catch (SendFailedException e) {
            throw EmailException.invalidAddress();
        } catch (Exception e) {
            logger.error("sendBySSL Exception", e);
            throw EmailException.unKnowError();
        }
    }

    private static void config(MimeMessage message, EmailParam param) throws MessagingException, UnsupportedEncodingException {
        // 收件人
        for (String recipient : param.getRecipients()) {
            if (StringTools.isNotBlank(recipient)) {
                InternetAddress to = new InternetAddress(recipient);
                message.addRecipient(Message.RecipientType.TO, to);
            }
        }

        // 抄送
        if (CollectionUtils.isNotEmpty(param.getCarbonCopies())) {
            for (String carbonCopy : param.getCarbonCopies()) {
                if (StringTools.isNotBlank(carbonCopy)) {
                    message.addRecipient(Message.RecipientType.CC, new InternetAddress(carbonCopy));
                }
            }
        }

        message.setSubject(param.getSubject());
        message.setSentDate(DateTimeTools.now());
        BodyPart mbp = new MimeBodyPart();
        mbp.setContent(param.getContent(), CHARSET_UTF_8);
        Multipart mm = new MimeMultipart();
        mm.addBodyPart(mbp);
        message.setContent(mm);

        if (CollectionUtils.isNotEmpty(param.getAttachments())) {
            for (File attachment : param.getAttachments()) {
                mbp = new MimeBodyPart();
                DataSource source = new FileDataSource(attachment);
                mbp.setDataHandler(new DataHandler(source));
                mbp.setFileName(MimeUtility.encodeWord(attachment.getName()));
                mm.addBodyPart(mbp);
            }
        }
    }
    // endregion private methods

}
