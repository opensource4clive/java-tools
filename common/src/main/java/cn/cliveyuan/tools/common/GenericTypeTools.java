package cn.cliveyuan.tools.common;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 泛型工具
 *
 * @author Clive Yuan
 * @date 2020/06/22
 */
@Slf4j
public class GenericTypeTools {

    private GenericTypeTools() {
    }

    /**
     * 获取父类的泛型类
     *
     * @param clazz 当前类
     * @param index 需要获取的泛型类下标
     * @return
     */
    public static Class<?> getSuperClassGenericType(final Class<?> clazz, final int index) {
        Type genType = clazz.getGenericSuperclass();
        if (!(genType instanceof ParameterizedType)) {
            log.warn("Warn: {}'s superclass not ParameterizedType", clazz.getSimpleName());
            return Object.class;
        }
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        if (index >= params.length || index < 0) {
            log.warn("Warn: Index: {}, Size of {}'s Parameterized Type: {} .", index, clazz.getSimpleName(), params.length);
            return Object.class;
        }
        if (!(params[index] instanceof Class)) {
            log.warn("Warn: {} not set the actual class on superclass generic parameter", clazz.getSimpleName());
            return Object.class;
        }
        return (Class<?>) params[index];
    }
}
