package cn.cliveyuan.tools.common;

import cn.cliveyuan.tools.common.bean.ValidationResult;
import org.apache.commons.collections4.CollectionUtils;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.groups.Default;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 验证工具
 *
 * @author clive
 * Created on 2018/07/24
 * @since 1.0
 */
public class ValidateTools {

    private ValidateTools() {
    }

    private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    /**
     * 正则匹配
     *
     * @param text
     * @param regex
     */
    public static boolean matches(String text, String regex) {
        if (StringTools.isBlank(text) || StringTools.isBlank(regex))
            return false;
        return Pattern.compile(regex).matcher(text).matches();
    }

    /**
     * 匹配年龄
     *
     * @param age
     */
    public static boolean isAge(int age) {
        return range(1, 200, age);
    }

    /**
     * 匹配姓名
     *
     * @param name
     */
    public static boolean isName(String name) {
        if (StringTools.isBlank(name)) return false;
        int length = name.length();
        if (isChineseChar(name))
            return range(1, 15, length);
        else
            return range(1, 30, length);
    }

    /**
     * 判断中文字符(包括汉字和符号)
     *
     * @param text
     */
    public static boolean isChineseChar(String text) {
        return matches(text, "^[\u0391-\uFFE5]+$");
    }

    /**
     * 匹配汉字
     *
     * @param text
     */
    public static boolean isChinese(String text) {
        return matches(text, "^[\u4e00-\u9fa5]+$");
    }

    /**
     * 判断英文字母(a-zA-Z)
     *
     * @param text
     */
    public static boolean isLetter(String text) {
        return matches(text, "^[A-Za-z]+$");
    }

    /**
     * 匹配正整数
     *
     * @param str
     */
    public static boolean isDigits(String str) {
        return matches(str, "^[0-9]*$");
    }

    /**
     * 匹配正浮点数
     *
     * @param str
     */
    public static boolean isFloat(String str) {
        return matches(str, "^[-\\+]?\\d+(\\.\\d+)?$");
    }


    /**
     * 匹配数值类型，包括整数和浮点数
     *
     * @param str
     */
    public static boolean isNumeric(String str) {
        return isFloat(str) || isDigits(str);
    }

    /**
     * 匹配URL地址
     *
     * @param str
     */
    public static boolean isUrl(String str) {
        return matches(str, "^http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w-./?%&=]*)?$");
    }

    /**
     * 最小
     *
     * @param min
     * @param value
     */
    public static boolean min(int min, int value) {
        return value >= min;
    }

    /**
     * 最大
     *
     * @param max
     * @param value
     */
    public static boolean max(int max, int value) {
        return value <= max;
    }

    /**
     * 大小范围
     *
     * @param min
     * @param max
     * @param value
     */
    public static boolean range(int min, int max, int value) {
        return min(min, value) && max(max, value);
    }

    /**
     * 是否大于等于最小长度
     *
     * @param min
     * @param str
     */
    public static boolean minLength(int min, String str) {
        if (Objects.isNull(str)) return false;
        return min(min, str.length());
    }

    /**
     * 是否小于等于最大长度
     *
     * @param max
     * @param str
     */
    public static boolean maxLength(int max, String str) {
        if (Objects.isNull(str)) return false;
        return max(max, str.length());
    }

    /**
     * 长度范围
     *
     * @param min 最小
     * @param max 最大
     * @param str 字符串
     */
    public static boolean rangeLength(int min, int max, String str) {
        return minLength(min, str) && maxLength(max, str);
    }

    /**
     * 手机号校验
     * @param text
     */
    public static boolean isMobile(String text) {
        if (StringTools.isBlank(text) || text.length() != 11)
            return false;
        return matches(text, "^[1]([3-9])[0-9]{9}$");
    }
    //邮箱

    /**
     * 匹配Email地址
     *
     * @param str
     */
    public static boolean isEmail(String str) {
        return matches(str, "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
    }

    /**
     * 验证银行卡
     * 15-19位数字
     *
     * @param text
     */
    public static boolean isBankCardNo(String text) {
        if (!isDigits(text)) return false;
        return range(15, 19, text.length());
    }

    /**
     * 身份证号码验证
     *
     * @param text
     */
    public static boolean isIdCardNo(String text) {
        return IDCardTools.isIDCard(text);
    }

    public static <T> void assertValidateEntity(T obj) {
        ValidationResult validationResult = ValidateTools.validateEntity(obj);
        AssertTools.isTrue(!validationResult.isHasErrors(), validationResult.getMessage());
    }
    public static <T> void assertValidateEntityWithoutFieldName(T obj) {
        ValidationResult validationResult = ValidateTools.validateEntity(obj);
        AssertTools.isTrue(!validationResult.isHasErrors(), validationResult.getMessageWithoutFieldName());
    }

    /**
     * 校验实体，返回实体所有属性的校验结果
     *
     * @param obj
     * @param <T>
     * @return
     */
    public static <T> ValidationResult validateEntity(T obj) {
        //解析校验结果
        Set<ConstraintViolation<T>> validateSet = VALIDATOR.validate(obj, Default.class);
        return buildValidationResult(validateSet);
    }

    /**
     * 校验指定实体的指定属性是否存在异常
     *
     * @param obj
     * @param propertyName
     * @param <T>
     * @return
     */
    public static <T> ValidationResult validateProperty(T obj, String propertyName) {
        Set<ConstraintViolation<T>> validateSet = VALIDATOR.validateProperty(obj, propertyName, Default.class);
        return buildValidationResult(validateSet);
    }

    /**
     * 将异常结果封装返回
     *
     * @param validateSet
     * @param <T>
     * @return
     */
    private static <T> ValidationResult buildValidationResult(Set<ConstraintViolation<T>> validateSet) {
        ValidationResult validationResult = new ValidationResult();
        if (CollectionUtils.isNotEmpty(validateSet)) {
            validationResult.setHasErrors(true);
            Map<String, String> errorMsgMap = new HashMap<>();
            for (ConstraintViolation<T> constraintViolation : validateSet) {
                errorMsgMap.put(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
            }
            validationResult.setErrorMsg(errorMsgMap);
        }
        return validationResult;
    }

    public static boolean isIp(String ipAddress) {
        Pattern pattern = Pattern.compile("([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}");
        Matcher matcher = pattern.matcher(ipAddress);
        return matcher.matches();
    }
}
