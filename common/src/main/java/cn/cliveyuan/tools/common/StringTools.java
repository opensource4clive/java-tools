package cn.cliveyuan.tools.common;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具
 *
 * @author clive
 * Created on 2018/07/23
 * @since 1.0
 */
public class StringTools extends StringUtils {

    private static final Pattern PATTERN = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
    public static final String EMPTY_JSON = "{}";
    public static final char C_BACKSLASH = '\\';
    public static final char C_DELIM_START = '{';

    private StringTools() {
    }

    public static boolean hasLength(String str) {
        return (str != null && str.length() > 0);
    }

    /**
     * 获取字符串的unicode编码
     *
     * @param str 字符串
     * @return unicode
     */
    public static String stringToUnicode(String str) {
        if (Objects.isNull(str)) {
            return null;
        }
        char[] chars = str.toCharArray();
        StringBuilder returnStr = new StringBuilder();
        for (char aChar : chars) {
            returnStr.append("\\u")
                    .append(Integer.toString(aChar, 16));
        }
        return returnStr.toString();
    }

    /**
     * Unicode转汉字字符串
     *
     * @param str unicode
     * @return 字符串
     */
    public static String unicodeToString(String str) {


        Matcher matcher = PATTERN.matcher(str);
        char ch;
        while (matcher.find()) {
            //group 6728
            String group = matcher.group(2);
            //ch:'木' 26408
            ch = (char) Integer.parseInt(group, 16);
            //group1 \u6728
            String group1 = matcher.group(1);
            str = str.replace(group1, ch + "");
        }
        return str;
    }


    /**
     * Check whether the given CharSequence has actual text.
     * More specifically, returns {@code true} if the string not {@code null},
     * its length is greater than 0, and it contains at least one non-whitespace character.
     * <p><pre class="code">
     * StringUtils.hasText(null) = false
     * StringUtils.hasText("") = false
     * StringUtils.hasText(" ") = false
     * StringUtils.hasText("12345") = true
     * StringUtils.hasText(" 12345 ") = true
     * </pre>
     *
     * @param str the CharSequence to check (may be {@code null})
     * @return {@code true} if the CharSequence is not {@code null},
     * its length is greater than 0, and it does not contain whitespace only
     * @see Character#isWhitespace
     */
    public static boolean hasText(String str) {
        if (!hasLength(str)) {
            return false;
        }
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    /**
     * map转查询字符串
     * 如: a=1&b=2&c=3&...
     *
     * @param data
     */
    public static String map2QueryStr(Map<String, String> data) {
        if (MapTools.isEmpty(data)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        data.forEach((k, v) ->
                sb.append(k)
                        .append("=")
                        .append(v)
                        .append("&")
        );
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public static Map<String, String> queryStr2Map(String query) {
        Map<String, String> map = new HashMap();
        if (isBlank(query)) {
            return map;
        }
        String[] sections = query.split("&");
        for (String section : sections) {
            String[] pairs = section.split("=");
            if (pairs.length >= 2) {
                map.put(pairs[0], pairs[1]);
            } else {
                map.put(pairs[0], "");
            }
        }
        return map;
    }

    /**
     * map转cookie字符串
     * 如: a=1;b=2;c=3;...
     *
     * @param data
     */
    public static String map2CookieStr(Map<String, String> data) {
        if (MapTools.isEmpty(data)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        data.forEach((k, v) ->
                sb.append(k)
                        .append("=")
                        .append(v)
                        .append(";")
        );
        return sb.toString();
    }

    /**
     * 超出截断
     * 返回的字符串长度是包括后缀的
     *
     * <p>
     * <pre>
     *         substringIfOverflow("CliveYuan",10) -> "CliveYuan"
     *         substringIfOverflow("CliveYuan",5) -> "Clive"
     *     </pre>
     * </p>
     *
     * @param str       字符串
     * @param maxLength 最大长度
     */
    public static String substringIfOverflow(String str, int maxLength) {
        if (Objects.isNull(str)) {
            return null;
        }
        if (ValidateTools.maxLength(maxLength, str)) {
            return str;
        }
        return str.substring(0, maxLength);
    }

    /**
     * str to map
     * k1:v1\n
     * k2:v2\n
     *
     * @param str
     */
    public static Map<String, String> str2Map(String str) {
        Map<String, String> map = new HashMap();
        if (Objects.isNull(str)) {
            return map;
        }
        String[] lines = str.split("\n");
        for (String line : lines) {
            if (StringTools.isBlank(line)) {
                continue;
            }
            int index = line.indexOf(":");
            if (index < 0) {
                map.put(line.trim(), StringTools.EMPTY);
                continue;
            }
            map.put(line.substring(0, index).trim(), line.substring(index + 1, line.length()).trim());
        }
        return map;
    }

    /**
     * URL编码
     *
     * @param text text
     */
    public static String urlEncode(String text) {
        try {
            return URLEncoder.encode(text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    /**
     * URL解码
     *
     * @param text text
     */
    public static String urlDecode(String text) {
        try {
            return URLDecoder.decode(text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    /**
     * Returns an array with strings between start and end.
     *
     * @param value input
     * @param start start
     * @param end   end
     * @return Array containing different parts between start and end.
     */

    public static String[] between(final String value, final String start, final String end) {
        AssertTools.notNull(value, "'value' should be not null.");
        AssertTools.notNull(start, "'start' should be not null.");
        AssertTools.notNull(end, "'end' should be not null.");
        String[] parts = value.split(end);
        return Arrays.stream(parts)
                .filter(subPart -> subPart.contains(start))
                .map(subPart -> subPart.substring(subPart.indexOf(start) + start.length()))
                .toArray(String[]::new);
    }

    /**
     * Returns first matched string between start and end.
     *
     * @param value input
     * @param start start
     * @param end   end
     */
    public static String firstBetween(final String value, final String start, final String end) {
        String[] betweenArray = between(value, start, end);
        if (ArrayTools.isNotEmpty(betweenArray)) {
            return betweenArray[0];
        }
        return EMPTY;
    }

    /**
     * 格式化字符串<br>
     * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
     * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
     * 例：<br>
     * 通常使用：format("this is {} for {}", "a", "b") -> this is a for b<br>
     * 转义{}： format("this is \\{} for {}", "a", "b") -> this is \{} for a<br>
     * 转义\： format("this is \\\\{} for {}", "a", "b") -> this is \a for b<br>
     *
     * @param strPattern 字符串模板
     * @param argArray   参数列表
     * @return 结果
     */
    public static String format(final String strPattern, final Object... argArray) {
        if (StringTools.isEmpty(strPattern) || ArrayTools.isEmpty(argArray)) {
            return strPattern;
        }
        final int strPatternLength = strPattern.length();

        // 初始化定义好的长度以获得更好的性能
        StringBuilder sbuf = new StringBuilder(strPatternLength + 50);

        int handledPosition = 0;
        int delimIndex;// 占位符所在位置
        for (int argIndex = 0; argIndex < argArray.length; argIndex++) {
            delimIndex = strPattern.indexOf(EMPTY_JSON, handledPosition);
            if (delimIndex == -1) {
                if (handledPosition == 0) {
                    return strPattern;
                } else { // 字符串模板剩余部分不再包含占位符，加入剩余部分后返回结果
                    sbuf.append(strPattern, handledPosition, strPatternLength);
                    return sbuf.toString();
                }
            } else {
                if (delimIndex > 0 && strPattern.charAt(delimIndex - 1) == C_BACKSLASH) {
                    if (delimIndex > 1 && strPattern.charAt(delimIndex - 2) == C_BACKSLASH) {
                        // 转义符之前还有一个转义符，占位符依旧有效
                        sbuf.append(strPattern, handledPosition, delimIndex - 1);
                        sbuf.append(argArray[argIndex]);
                        handledPosition = delimIndex + 2;
                    } else {
                        // 占位符被转义
                        argIndex--;
                        sbuf.append(strPattern, handledPosition, delimIndex - 1);
                        sbuf.append(C_DELIM_START);
                        handledPosition = delimIndex + 1;
                    }
                } else {
                    // 正常占位符
                    sbuf.append(strPattern, handledPosition, delimIndex);
                    sbuf.append(argArray[argIndex]);
                    handledPosition = delimIndex + 2;
                }
            }
        }
        // 加入最后一个占位符后所有的字符
        sbuf.append(strPattern, handledPosition, strPattern.length());

        return sbuf.toString();
    }

    /**
     * 去空格
     */
    public static String trim(String str) {
        return (Objects.isNull(str) ? EMPTY : str.trim());
    }

    /**
     * 截取字符串
     *
     * @param str    字符串
     * @param length 长度
     * @return 结果
     */
    public static String substringLength(final String str, int length) {
        return substringLength(str, 0, length);
    }

    /**
     * 截取字符串
     *
     * @param str    字符串
     * @param start  开始
     * @param length 长度
     * @return 结果
     */
    public static String substringLength(final String str, int start, int length) {
        return substring(str, start, start + length);
    }

    /**
     * 截取字符串
     *
     * @param str   字符串
     * @param start 开始
     * @return 结果
     */
    public static String substring(final String str, int start) {
        if (str == null) {
            return EMPTY;
        }

        if (start < 0) {
            start = str.length() + start;
        }

        if (start < 0) {
            start = 0;
        }
        if (start > str.length()) {
            return EMPTY;
        }

        return str.substring(start);
    }

    /**
     * 截取字符串
     *
     * @param str   字符串
     * @param start 开始
     * @param end   结束
     * @return 结果
     */
    public static String substring(final String str, int start, int end) {
        if (str == null) {
            return EMPTY;
        }

        if (end < 0) {
            end = str.length() + end;
        }
        if (start < 0) {
            start = str.length() + start;
        }

        if (end > str.length()) {
            end = str.length();
        }

        if (start > end) {
            return EMPTY;
        }

        if (start < 0) {
            start = 0;
        }
        if (end < 0) {
            end = 0;
        }

        return str.substring(start, end);
    }

    /**
     * 删除字符串中的html标签
     *
     * @param htmlStr 字符串
     * @return
     */
    public static String delHtmlTag(String htmlStr) {
        String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式
        String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式
        String regEx_html = "<[^>]+>"; //定义HTML标签的正则表达式

        Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
        Matcher m_script = p_script.matcher(htmlStr);
        htmlStr = m_script.replaceAll(""); //过滤script标签

        Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
        Matcher m_style = p_style.matcher(htmlStr);
        htmlStr = m_style.replaceAll(""); //过滤style标签

        Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
        Matcher m_html = p_html.matcher(htmlStr);
        htmlStr = m_html.replaceAll(""); //过滤html标签

        return htmlStr.trim(); //返回文本字符串
    }


}
