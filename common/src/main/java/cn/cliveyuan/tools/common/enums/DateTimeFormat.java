package cn.cliveyuan.tools.common.enums;

/**
 * 日期时间格式常量
 *
 * @author clive
 * Created on 2018/07/23
 * @since 1.0
 */
public enum DateTimeFormat {

    DATE_0("yyyyMMdd"),
    DATE_1("yyyy-MM-dd"),
    DATE_2("yyyy/MM/dd"),
    DATE_3("yyyy.MM.dd"),

    TIME_0("HHmmss"),
    TIME_1("HH:mm:ss"),
    TIME_2("HH:mm"),

    DATE_TIME_0("yyyyMMddHHmmss"),
    DATE_TIME_1("yyyy-MM-dd HH:mm:ss"),
    DATE_TIME_2("yyyy-MM-dd HH:mm"),
    DATE_TIME_3("yyyy/MM/dd HH:mm:ss"),
    DATE_TIME_4("yyyy/MM/dd HH:mm");

    DateTimeFormat(String format) {
        this.format = format;
    }

    private final String format;

    public String getFormat() {
        return format;
    }
}
