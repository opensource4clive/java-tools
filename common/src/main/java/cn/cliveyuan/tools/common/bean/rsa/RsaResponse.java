package cn.cliveyuan.tools.common.bean.rsa;

import lombok.Builder;
import lombok.Data;

/**
 * @author Clive Yuan
 * @date 2021/09/03
 */
@Data
@Builder
public class RsaResponse {

    /**
     * 是否成功
     */
    private boolean success;
    /**
     * 错误消息
     */
    private String msg;
    /**
     * 数据
     */
    private byte[] data;
}
