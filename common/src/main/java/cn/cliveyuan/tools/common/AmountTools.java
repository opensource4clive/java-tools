package cn.cliveyuan.tools.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * 金额工具
 *
 * @author Clive Yuan
 * @date 2022/06/25
 * @since 4.0.4
 */
public class AmountTools {
    private AmountTools() {
    }

    private static final BigDecimal ONE_HUNDRED = new BigDecimal("100");

    /**
     * 元转分
     *
     * @param yuan 元
     * @return 分
     */
    public static Integer yuan2Fen(BigDecimal yuan) {
        return getPositiveAmt(yuan).multiply(ONE_HUNDRED).intValue();
    }

    /**
     * 分转元
     *
     * @param fen 分
     * @return 元
     */
    public static BigDecimal fen2Yuan(Integer fen) {
        return getPositiveAmt(BigDecimal.valueOf(fen)).divide(ONE_HUNDRED, 2, RoundingMode.HALF_UP);
    }

    /**
     * 分转元
     *
     * @param fen          分
     * @param scale        精度
     * @param roundingMode 舍入模式
     * @return 元
     */
    public static BigDecimal fen2Yuan(Integer fen, int scale, RoundingMode roundingMode) {
        return getPositiveAmt(BigDecimal.valueOf(fen)).divide(ONE_HUNDRED, scale, roundingMode);
    }

    /**
     * 获取正数金额
     *
     * @param bigDecimal 金额
     * @return 零或正数金额
     */
    public static BigDecimal getPositiveAmt(BigDecimal bigDecimal) {
        return Objects.nonNull(bigDecimal) && bigDecimal.compareTo(BigDecimal.ZERO) > 0 ? bigDecimal : BigDecimal.ZERO;
    }
}
