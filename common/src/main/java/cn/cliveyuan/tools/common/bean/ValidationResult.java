package cn.cliveyuan.tools.common.bean;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.Map;
import java.util.StringJoiner;

/**
 * @author Clive Yuan
 * @date 2021/06/09
 */
public class ValidationResult {

    /**
     * 是否有异常
     */
    private boolean hasErrors;

    /**
     * 异常消息记录
     */
    private Map<String, String> errorMsg;

    /**
     * 获取异常消息组装
     *
     * @return
     */
    public String getMessage() {
        if (errorMsg == null || errorMsg.isEmpty()) {
            return StringUtils.EMPTY;
        }
        StringJoiner msgJoiner = new StringJoiner("\r\n");
        errorMsg.forEach((key, value) -> msgJoiner.add(MessageFormat.format("{0}: {1}", key, value)));
        return msgJoiner.toString();
    }
    public String getMessageWithoutFieldName() {
        if (errorMsg == null || errorMsg.isEmpty()) {
            return StringUtils.EMPTY;
        }
        StringJoiner msgJoiner = new StringJoiner("\r\n");
        errorMsg.forEach((key, value) -> msgJoiner.add(value));
        return msgJoiner.toString();
    }


    public boolean isHasErrors() {
        return hasErrors;
    }

    public void setHasErrors(boolean hasErrors) {
        this.hasErrors = hasErrors;
    }

    public Map<String, String> getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(Map<String, String> errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "hasErrors=" + hasErrors +
                ", errorMsg=" + errorMsg +
                '}';
    }
}
