package cn.cliveyuan.tools.common;

/**
 * 脱敏工具
 *
 * @author Clive Yuan
 * @date 2021/10/10
 * @since 4.0.4
 */
public class DesensitizationTools {

    /**
     * 脱敏人名
     * <p>
     * 张三 			-> 张* <br/>
     * 张三丰 		-> 张*丰 <br/>
     * 张 			-> 张 <br/>
     * 王二麻子 		-> 王**子 <br/>
     * 尼古拉斯·凯奇 	-> 尼*****奇 <br/>
     * </p>
     *
     * @param name 人名
     */
    public static String formatName(String name) {
        if (StringTools.isBlank(name) || name.length() < 2) return name;
        if (name.length() == 2) return name.charAt(0) + "*";
        return name.charAt(0) + getAppendStr(name.length() - 2) + name.charAt(name.length() - 1);
    }

    /**
     * 脱敏手机号
     * <p>
     * 13800138000 -> 138****8000
     * </p>
     *
     * @param mobile 手机号
     */
    public static String formatMobile(String mobile) {
        return formatToMaskMid(mobile, 3, 4);
    }

    /**
     * 脱敏身份证号
     * <p>
     * 510184199001018888 -> 51018**********888
     * </p>
     *
     * @param idCardNo 身份证号码
     */
    public static String formatIdCardNo(String idCardNo) {
        return formatToMaskMid(idCardNo, 5, 10);
    }


    /**
     * 脱敏中间部分
     * <p>
     * src="1234567890", start=3, xLength=4 -> 123****890
     * </p>
     *
     * @param src     原串
     * @param start   打码内容开始字符串下标(从0开始算)
     * @param xLength 打码内容长度
     */
    public static String formatToMaskMid(String src, int start, int xLength) {
        return formatToMask(src, start, xLength, "$1$3");
    }

    /**
     * 脱敏左右部分
     * <p>
     * src="1234567890", start=3, xLength=4 -> ***4567***
     * </p>
     *
     * @param src     原串
     * @param start   明文内容开始字符串下标(从0开始算)
     * @param xLength 明文内容长度
     */
    public static String formatToMaskAbout(String src, int start, int xLength) {
        return formatToMask(src, start, xLength, "$2");
    }

    /**
     * 号码串脱敏
     *
     * @param src     纯数字字符串
     * @param start   开始索引
     * @param xLength 脱敏长度($1$3)，保留长度($2)
     * @param fmt     脱敏格式：$1$3首尾保留，$2中间保留
     */
    public static String formatToMask(String src, int start, int xLength, String fmt) {
        if (StringTools.isEmpty(src)) {
            return StringTools.EMPTY;
        }
        int end = src.length() - start - xLength;

        if (start * xLength * end < 0) {
            // 返回原字符串
            return src;
        }
        String replace;
        if ("$2".equals(fmt)) {
            String s1 = getAppendStr(start);
            String s3 = getAppendStr(end);
            replace = s1 + "$2" + s3;
        } else {
            String s2 = getAppendStr(xLength);
            replace = "$1" + s2 + "$3";
        }
        // 修改这行代码可以自定义脱敏格式
        return src.replaceAll("(\\w{" + start + "})(\\w{" + xLength + "})(\\w{" + end + "})", replace);
    }

    private static String getAppendStr(int xLength) {
        StringBuilder r = new StringBuilder();
        for (int i = 0; i < xLength; i++) {
            r.append("*");
        }
        return r.toString();
    }


}
