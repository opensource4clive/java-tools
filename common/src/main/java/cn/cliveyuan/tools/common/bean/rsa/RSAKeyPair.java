package cn.cliveyuan.tools.common.bean.rsa;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Clive on 2020/02/22.
 */
@Data
@Builder
public class RSAKeyPair implements Serializable {
    /**
     * 公钥 (base64格式)
     */
    private String publicKey;
    /**
     * 私钥 (base64格式)
     */
    private String privateKey;
}
