package cn.cliveyuan.tools.common.exception;

/**
 * 基础异常
 *
 * @author clive
 * Created on 2018/07/23
 * @since 1.0
 */
public class BaseException extends RuntimeException {

    public BaseException(String message) {
        super(message);
    }

    public BaseException(int code, String message) {
        super(message);
        this.code = code;
    }

    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
