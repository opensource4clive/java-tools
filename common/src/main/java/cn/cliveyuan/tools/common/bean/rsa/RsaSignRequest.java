package cn.cliveyuan.tools.common.bean.rsa;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Created by Clive on 2020/02/22.
 */
@Data
@Builder
public class RsaSignRequest implements Serializable {

    /**
     * 需要验证签名的数据字符串 (必传)
     */
    private byte[] data;

    /**
     * RSA公钥  (必传)
     */
    private byte[] key;

    /**
     * 是否验签: true->验签, false->签名
     */
    @Builder.Default
    private boolean isVerifySign = false;

    /**
     * 签名字符串(验签时必传)
     */
    private byte[] sign;

    /**
     * 密钥算法, 默认 RSA
     */
    @Builder.Default
    private String keyAlgorithm = "RSA";

    /**
     * 签名算法, 默认 SHA256withRSA
     */
    @Builder.Default
    private String signatureAlgorithm = "SHA256withRSA";

    /**
     * 是否打印错误日志
     */
    @Builder.Default
    private boolean printErrorLog = true;
}
