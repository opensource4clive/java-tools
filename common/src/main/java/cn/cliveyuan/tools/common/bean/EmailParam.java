package cn.cliveyuan.tools.common.bean;

import lombok.Builder;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Clive Yuan
 * @date 2021/06/09
 */
@Data
@Builder
public class EmailParam {
    /**
     * 邮件发送主机, 如 smtp.exmail.qq.com
     */
    @NotBlank
    private String host;
    /**
     * 发送端口
     */
    private Integer port;
    /**
     * 账号
     */
    @NotBlank
    private String account;
    /**
     * 密码
     */
    @NotBlank
    private String password;
    /**
     * 收件人
     */
    @Builder.Default
    @NotEmpty
    private List<String> recipients = new ArrayList<>();
    /**
     * 抄送人
     */
    @Builder.Default
    private List<String> carbonCopies = new ArrayList<>();
    /**
     * 主题
     */
    @NotBlank
    private String subject;
    /**
     * 内容
     */
    @NotBlank
    private String content;
    /**
     * 附件
     */
    @Builder.Default
    private List<File> attachments = new ArrayList<>();
    /**
     * 通过SSL发送
     */
    private boolean isSSL;
    /**
     * 调试
     */
    private boolean debug;
}
