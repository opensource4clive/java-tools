package cn.cliveyuan.tools.common;

import cn.cliveyuan.tools.common.enums.OSEnum;

/**
 * 系统工具
 *
 * @author Clive Yuan
 * @date 2022/06/25
 * @since 4.0.4
 */
public class SystemTools {

    /**
     * 获取操作系统类型
     *
     * @return OSEnum
     */
    public static OSEnum getOs() {
        String osName = System.getProperty("os.name");
        if (StringTools.isBlank(osName)) return OSEnum.UNKNOWN;
        osName = osName.toLowerCase();
        if (osName.contains("windows")) {
            return OSEnum.WINDOWS;
        } else if (osName.contains("mac")) {
            return OSEnum.MAC;
        } else if (osName.contains("linux")) {
            return OSEnum.LINUX;
        } else {
            return OSEnum.OTHER;
        }
    }

    /**
     * 是否为苹果操作系统
     */
    public static boolean isMacOs() {
        return OSEnum.MAC.equals(getOs());
    }

    /**
     * 是否为Windows操作系统
     */
    public static boolean isWindowsOs() {
        return OSEnum.WINDOWS.equals(getOs());
    }

    /**
     * 是否为Linux操作系统
     */
    public static boolean isLinuxOs() {
        return OSEnum.LINUX.equals(getOs());
    }
}
