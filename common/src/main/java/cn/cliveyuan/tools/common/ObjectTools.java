package cn.cliveyuan.tools.common;

import java.util.Objects;

/**
 * 对象工具
 *
 * @author Clive Yuan
 * @date 2022/06/25
 * @since 4.0.4
 */
public class ObjectTools {
    private ObjectTools() {
    }

    /**
     * 如果不为空则返回, 为空则返回默认值
     *
     * @param value        值
     * @param defaultValue 默认值
     * @param <T>          类型参数
     */
    public static <T> T getOrDefault(T value, T defaultValue) {
        return Objects.nonNull(value) ? value : defaultValue;
    }

    /**
     * 如果字符串不为空白则返回, 为空白则返回默认值
     *
     * @param value        值
     * @param defaultValue 默认值
     */
    public static String getOrDefaultString(String value, String defaultValue) {
        return StringTools.isNoneBlank(value) ? value : defaultValue;
    }

    /**
     * 如果字符串不为空白则返回, 为空白则返回空白
     *
     * @param value 值
     */
    public static String getOrDefaultString(String value) {
        return getOrDefaultString(value, StringTools.EMPTY);
    }
}
