package cn.cliveyuan.tools.common;

import java.util.Map;

/**
 * @author Clive Yuan
 * @date 2021/06/08
 */
public class MapTools {
    /**
     * map是否为空
     *
     * @param map map
     */
    public static boolean isEmpty(Map map) {
        return map == null || map.isEmpty();
    }

    /**
     * map是否不为空
     *
     * @param map map
     */
    public static boolean isNotEmpty(Map map) {
        return !isEmpty(map);
    }

}
