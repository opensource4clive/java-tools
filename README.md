# java-tools

## Description
Some commonly used methods in JAVA 

## Required
* JDK1.8+

## How to use
`last.version`=   <a href="https://search.maven.org/search?q=com.gitee.opensource4clive%20AND%20java-tools">
          <img alt="maven" src="https://img.shields.io/maven-central/v/com.gitee.opensource4clive/java-tools.svg?style=flat-square">
        </a>
```xml
        <dependency>
            <groupId>com.gitee.opensource4clive</groupId>
            <artifactId>java-tools</artifactId>
            <version>${last.version}</version>
        </dependency>
```
## Feature List
* [feature](doc/feature.md)

## Release Logs

### v4.0.6 2024-03-06
> sonatype.com: 代码扫描存在30个威胁,尝试解决后提交
- spring-boot-starter-data-redis > 2.7.18
- fastjson > 2.0.47
- commons-io.version > 2.15.1
- poi.version > 4.1.2
- okhttp > 4.10.0
- hibernate-validator > 7.0.2.Final
- jakarta.el > 4.0.2
- remove: zip4j

### v4.0.5 2023-12-29
* Captcha 实现序列化接口 `Serializable`

### v4.0.4 2022-06-28
* https://gitee.com/opensource4clive/java-tools/issues/I4BXZU
* https://gitee.com/opensource4clive/java-tools/issues/I4C4PP
* https://gitee.com/opensource4clive/java-tools/issues/I5E2QW

### v4.0.3 2021-09-15
* add redis module to support redis cache manager

### v4.0.2 2021-09-06
* refine AESTools,DateTimeTools,RSATools,HttpClientTools
* see https://gitee.com/opensource4clive/java-tools/issues/I4656Z

### v4.0.1 2021-06-24
* refine IpTools

### v4.0.0 2021-06-16
* slim tools: split to common, httpclient, poi, web

### v3.1.1 2021-06-15
* Refine IpTools

### v3.1.0 2020-06-16
* add tools: SqlTools,ArrayTools
* add methods: format,substring,trim in StringTools
* refine ExcelTools, CsvTools
* remove old Captcha

### v3.0.7 2020-06-11
* upgrade fastjson to 1.2.70 and open SafeMode

### v3.0.6 2020-04-10
* add ignoreSslCertificate method in HttpClientTools and JsoupTools

### v3.0.5 2020-03-23
* upgrade fastjson to 1.2.67, refine HttpClientTools and JsoupTools

### v3.0.4 2020-02-29
* fix bug: CaptchaTools, DateTimeTools

### v3.0.3 2020-02-27
* IpTools: don't get local ip

### v3.0.2 2020-02-22
* RSATools: add RSATools

### v3.0.1 2020-02-17
* CaptchaTools: add captcha type: mathAdd

### v3.0.0 2019-10-30
* modify the groupId to com.gitee.opensource4clive and deploy to sonatype.org

### v2.1.0 2019-10-28
* add ExcelReader and ExcelWriter, support multi-sheet read and write

### v2.0.11 2019-08-16
* remove CommandExecuteTools.java, just use "zt-exec"

### v2.0.10 2019-08-09
* add CommandExecuteTools.java

### v2.0.9 2019-08-02
* fix bug for EmailTool on ssl

### v2.0.8 2019-07-13
* new feature for upload tool

### v2.0.7 2019-07-13
* fix file upload tool

### v2.0.7 2019-06-24
* refine file upload tool

### v2.0.6 2019-06-16
* modify captcha tool

### v2.0.5 2019-06-15
* fix ip tools bug

### v2.0.4 2019-05-26
* fix Http tools bug

### v2.0.3 2019-01-02
* HttpClientTools add statusCode

### v2.0.2 2018-11-17
* change the zip implement to zip4j
* add method 'between' in StringTools.java and refine method 'str2Map'

### v2.0.1 2018-11-27
* add execute method
    * JsoupTools
    * HttpClientTools

### v2.0.0 2018-11-26
* Easy to use (may not compatible with lower versions)
    * JsoupTools
    * HttpClientTools
    * EmailTools

### v1.0.14 2018-11-08
* HttpClientTools: add method

### v1.0.13 2018-11-01
* fix excel parse bug: TableAbstract.java:34

### v1.0.12 2018-11-01
* fix file upload bug: FileUploadTools.java:67

### v1.0.11 2018-10-30
* remove JsoupTool default header

### v1.0.10 2018-09-28
* fix captcha verify bug

### v1.0.9 2018-08-02
* remove log implement->log4j

### v1.0.8 2018-08-01
* refine excel,csv

### v1.0.7 2018-07-31
* bug fix

### v1.0.5 2018-07-30
* release first version
