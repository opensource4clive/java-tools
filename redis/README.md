# java-tools-redis

## Description
use redis cache as cacheManager

## Steps
### 1. Add Dependency.
```xml
<dependency>
    <groupId>com.gitee.opensource4clive</groupId>
    <artifactId>java-tools-redis</artifactId>
    <version>${java-tools.version}</version>
</dependency>
```
### 2. Create `RedisConfiguration.java`
```java
import cn.cliveyuan.tools.redis.RedisCacheConfig;
import cn.cliveyuan.tools.redis.RedisConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.time.Duration;

import static cn.cliveyuan.tools.redis.RedisCacheManager.CONFIG_LIST;

@Configuration
@Import(RedisConfig.class)
public class RedisConfiguration {

    static {
        // admin session (12小时失效)
        CONFIG_LIST.add(RedisCacheConfig.builder().name("adminSession").expireAfterWrite(Duration.ofHours(12)).build());
        // user session (2小时失效)
        CONFIG_LIST.add(RedisCacheConfig.builder().name("userSession").expireAfterWrite(Duration.ofHours(2)).build());
        // captcha (1分钟失效)
        CONFIG_LIST.add(RedisCacheConfig.builder().name("captcha").expireAfterWrite(Duration.ofMinutes(1)).build());
        // config (24小时失效)
        CONFIG_LIST.add(RedisCacheConfig.builder().name("config").expireAfterWrite(Duration.ofHours(24)).build());
        // authorization (4小时失效)
        CONFIG_LIST.add(RedisCacheConfig.builder().name("authorization").expireAfterWrite(Duration.ofHours(4)).build());
    }
}

```

### 3. Modify `CacheConfiguration.java`
```java
@Configuration
public class CacheConfiguration {

    @Bean
    public CacheManager cacheManager() {
        return redisCacheManager(); // modify to redisCacheManager
    }

    // add redisService
    @Resource
    private RedisService redisService;
    // add redisCacheManager
    private RedisCacheManager redisCacheManager() {
        return new RedisCacheManager(redisService);
    }
}
```
### 4. Add Redis Config.
```yaml
# redis config
spring:
  redis:
    database: 14
    host: redis.servers.dev.ofc
    port: 6379
```
