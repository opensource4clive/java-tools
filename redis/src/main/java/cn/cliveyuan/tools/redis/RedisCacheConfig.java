package cn.cliveyuan.tools.redis;

import lombok.Builder;
import lombok.Data;

import java.time.Duration;

/**
 * Redis缓存配置
 *
 * @author Clive Yuan
 * @date 2021/09/14
 */
@Data
@Builder
public class RedisCacheConfig {

    /**
     * 缓存名
     */
    private String name;

    /**
     * 失效时间 默认30分钟
     */
    @Builder.Default
    private Duration expireAfterWrite = Duration.ofMinutes(30);
}
