package cn.cliveyuan.tools.redis;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Redis 缓存管理器
 *
 * @author Clive Yuan
 * @date 2021/09/14
 */
public class RedisCacheManager implements CacheManager {

    public static final List<RedisCacheConfig> CONFIG_LIST = new ArrayList<>();

    private static final ConcurrentHashMap<String, Cache> CACHE_MAP = new ConcurrentHashMap<>();
    private static final Set<String> NAMES = new HashSet<>();
    private final RedisService redisService;

    public RedisCacheManager(RedisService redisService) {
        this.redisService = redisService;
        CONFIG_LIST.forEach(config -> CACHE_MAP.putIfAbsent(config.getName(), new RedisCache(config, redisService)));
    }

    @Override
    public Cache getCache(String name) {
        NAMES.add(name);
        Cache cache = CACHE_MAP.get(name);
        if (Objects.nonNull(cache)) {
            return cache;
        }
        cache = new RedisCache(RedisCacheConfig.builder().name(name).build(), redisService);
        CACHE_MAP.putIfAbsent(name, cache);
        return cache;
    }

    @Override
    public Collection<String> getCacheNames() {
        return NAMES;
    }
}
