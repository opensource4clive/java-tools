# Feature List

## common
* AESTools
* AmountTools
* ArrayTools
* AssertTools
* CsvTools
* DateTimeTools
* DesensitizationTools
* EmailTools
* FileTools
* GenericTypeTools
* IDCardTools
* IdTools
* MapTools
* MD5Tools
* ObjectTools
* ReflectTools
* RSATools
* StringTools
* SystemTools
* TableUtils
* ThreadTools
* ValidateTools
* XmlTools
* ZipTools

## httpclient
* HttpClientTools

## poi
* ExcelTools

## redis
* RedisService

## web
* IpTools
* CookieTools
* CaptchaTools
* VerifyCodeTools
* FileUploadTools
