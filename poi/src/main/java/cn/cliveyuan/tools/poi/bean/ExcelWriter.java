package cn.cliveyuan.tools.poi.bean;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Clive on 2019/10/28.
 */
@Data
@Builder
@Slf4j
public class ExcelWriter implements Serializable {

    /**
     * 文件路径
     */
    private String filePath;
    /**
     * 文件名 (不指定则随机生成)
     */
    private String fileName;
    /**
     * 文件类型
     */
    @Builder.Default
    private ExcelType fileType = ExcelType.XLSX;
    /**
     * sheet 内容列表
     */
    @Builder.Default
    private List<SheetContent> sheetContentList = new ArrayList<>();

    public ExcelWriter addSheetContent(String sheetName, String[] headers, Collection data) {
        this.sheetContentList.add(new SheetContent(sheetName, headers, data));
        return this;
    }

}
