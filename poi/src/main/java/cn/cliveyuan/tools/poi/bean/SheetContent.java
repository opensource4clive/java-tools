package cn.cliveyuan.tools.poi.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by Clive on 2019/10/28.
 */
@Data
public class SheetContent<T> implements Serializable {
    private String sheetName;
    private String[] headers;
    private Collection<T> data;

    public SheetContent(String sheetName, String[] headers, Collection<T> data) {
        this.sheetName = sheetName;
        this.headers = headers;
        this.data = data;
    }
}
