package cn.cliveyuan.tools.poi.bean;

/**
 * excel格式
 *
 * @author clive
 * Created on 2018/07/30
 * @since 1.0
 */
public enum ExcelType {
    XLS,
    XLSX,
    XLSM
}
