package cn.cliveyuan.tools.poi.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author Clive Yuan
 * @date 2020/06/19
 */
@Data
public class SheetData implements Serializable {
    private String sheetName;
    private List<Object> dataList;

    public SheetData(String sheetName, List<Object> dataList) {
        this.sheetName = sheetName;
        this.dataList = dataList;
    }
}
