package cn.cliveyuan.tools.poi;


import cn.cliveyuan.tools.poi.bean.ExcelReader;
import cn.cliveyuan.tools.poi.bean.ExcelWriter;
import cn.cliveyuan.tools.poi.bean.SheetContent;
import cn.cliveyuan.tools.poi.bean.SheetData;
import cn.cliveyuan.tools.poi.bean.SheetInfo;
import cn.cliveyuan.tools.poi.inner.ExcelInnerHelper;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * Excel 工具类
 *
 * @author Clive Yuan
 * @date 2021-06-09
 */
public class ExcelTools {

    private ExcelTools() {
    }

    /**
     * 读取Excel
     *
     * @param excelReader excelReader
     * @return
     */
    public static List<SheetData> read(ExcelReader excelReader) {
        if (excelReader.isSkipFirstRow()) {
            excelReader.setStartRowNo(1);
        }
        return ExcelInnerHelper.read(excelReader);
    }

    /**
     * 简单读取Excel, 读取名为'Sheet1'的数据
     *
     * @param filePath     文件路径
     * @param clazz        类
     * @param skipFirstRow 是否跳过首行
     * @param <T>          类泛型
     * @return
     */
    public static <T> List<T> readSimply(String filePath, Class<T> clazz, boolean skipFirstRow) {
        SheetInfo sheetInfo = new SheetInfo("Sheet1", clazz);
        List<SheetData> list = read(ExcelReader.builder()
                .pathname(filePath)
                .sheetInfoList(Collections.singletonList(sheetInfo))
                .skipFirstRow(skipFirstRow)
                .build());
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        SheetData sheetData = list.get(0);
        return JSONArray.parseArray(JSON.toJSONString(sheetData.getDataList()), clazz);
    }

    /**
     * 生成Excel
     *
     * @param excelWriter excelWriter
     * @return
     */
    public static File write(ExcelWriter excelWriter) {
        return ExcelInnerHelper.write(excelWriter);
    }

    /**
     * 简单生成单个sheetExcel
     *
     * @param filePath 文件绝对路径
     * @param headers  抬头
     * @param data     数据
     * @param <T>      数据类泛型
     * @return
     */
    public static <T> File writeSimply(String filePath, String[] headers, List<T> data) {
        SheetContent<T> sheetContent = new SheetContent<>("Sheet1", headers, data);
        return write(ExcelWriter.builder()
                .filePath(filePath)
                .sheetContentList(Collections.singletonList(sheetContent))
                .build());
    }

    /**
     * 读入excel文件，解析后返回
     * key: sheet名, value每行数据
     *
     * @param absoluteFilePath 文件绝对路径
     * @param sheetNames       指定的sheet名
     */
    public static Map<String, List<String[]>> readRawExcel(String absoluteFilePath, String... sheetNames) {
        return ExcelInnerHelper.readRawExcel(absoluteFilePath, sheetNames);
    }
}
