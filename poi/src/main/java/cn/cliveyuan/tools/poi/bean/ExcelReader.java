package cn.cliveyuan.tools.poi.bean;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clive on 2019/10/28.
 */
@Data
@Builder
@Slf4j
public class ExcelReader implements Serializable {

    /**
     * excel 全文件名 (包括扩展名, 如 /xxx/a/b/test.xlsx)
     */
    private String pathname;
    /**
     * sheet 信息列表
     */
    @Builder.Default
    private List<SheetInfo> sheetInfoList = new ArrayList<>();
    /**
     * 开始行号
     */
    private int startRowNo;
    /**
     * 跳过首行
     */
    private boolean skipFirstRow;

    public ExcelReader addSheetInfo(String sheetName, Class<?> clazz) {
        sheetInfoList.add(new SheetInfo(sheetName, clazz));
        return this;
    }
}
