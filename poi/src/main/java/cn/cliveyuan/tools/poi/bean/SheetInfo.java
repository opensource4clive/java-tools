package cn.cliveyuan.tools.poi.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Clive Yuan
 * @date 2020/06/19
 */
@Data
public class SheetInfo implements Serializable {
    private String sheetName;
    private Class clazz;

    public SheetInfo(String sheetName, Class clazz) {
        this.sheetName = sheetName;
        this.clazz = clazz;
    }
}
