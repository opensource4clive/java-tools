package cn.cliveyuan.tools.poi.exception;

import cn.cliveyuan.tools.common.exception.BaseException;

/**
 * excel异常
 *
 * @author clive
 * Created on 2018/07/25
 * @since 1.0
 */
public class ExcelException extends BaseException {

    public ExcelException(int code, String message) {
        super(code, message);
    }

    public static ExcelException failToMkdirs() {
        return new ExcelException(1000, "fail to create folder(s)");
    }

    public static ExcelException failToGenerateExcel() {
        return new ExcelException(1001, "fail to generate excel");
    }

    public static ExcelException excelReadError() {
        return new ExcelException(1002, "excel read error");
    }

    public static ExcelException failToReadSheet() {
        return new ExcelException(1003, "fail to read sheet");
    }

    public static ExcelException failToParseExcel() {
        return new ExcelException(1004, "fail to parse excel");
    }

    public static ExcelException notSupport(String extension) {
        return new ExcelException(1005, "not support this excel format:" + extension);
    }

    public static ExcelException dataConvertError() {
        return new ExcelException(1006, "data convert error");
    }
}
