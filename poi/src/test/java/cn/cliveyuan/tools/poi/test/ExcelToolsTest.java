package cn.cliveyuan.tools.poi.test;

import cn.cliveyuan.tools.poi.ExcelTools;
import cn.cliveyuan.tools.poi.bean.ExcelReader;
import cn.cliveyuan.tools.poi.bean.ExcelWriter;
import cn.cliveyuan.tools.poi.bean.SheetData;
import cn.cliveyuan.tools.poi.test.bean.ExcelIdea;
import cn.cliveyuan.tools.poi.test.bean.ExcelProfile;
import cn.cliveyuan.tools.poi.test.bean.ExcelWebsite;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author clive
 * Created on 2018/07/25
 * @since 1.0
 */
public class ExcelToolsTest {


    @Test
    public void writeAndRead() {

        String[] headers1 = {"编号", "名字", "手机号", "年龄"};
        String[] headers2 = {"name", "url"};
        String[] headers3 = {"ide", "lang",};

        List<ExcelProfile> data1 = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            data1.add(ExcelProfile.builder()
                    .no(i + 1)
                    .name("Clive-" + i)
                    .mobile("138001380" + i)
                    .age(i + 20)
                    .build()
            );
        }
        List<ExcelWebsite> data2 = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            data2.add(ExcelWebsite.builder()
                    .name("clive-" + i)
                    .url("cliveyuan.cn/" + i)
                    .build()
            );
        }
        List<ExcelIdea> data3 = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            data3.add(ExcelIdea.builder()
                    .ide("idea-" + i)
                    .lang("java-" + i)
                    .build()
            );
        }

        String filePath = ExcelToolsTest.class.getResource("/").getPath();
        File generatedFile = ExcelTools.write(ExcelWriter.builder()
                .filePath(filePath)
                .fileName("测试")
                .build()
                .addSheetContent("profile", headers1, data1)
                .addSheetContent("website", headers2, data2)
                .addSheetContent("idea", headers3, data3));
        Assert.assertNotNull(generatedFile);
        System.out.println("generatedFile:" + generatedFile.getAbsolutePath());

        List<SheetData> excelDataList = ExcelTools.read(ExcelReader
                .builder()
                .pathname(generatedFile.getAbsolutePath())
                .skipFirstRow(true)
                .build()
                .addSheetInfo("profile", ExcelProfile.class)
                .addSheetInfo("website", ExcelWebsite.class)
                .addSheetInfo("idea", ExcelIdea.class));
        Assert.assertFalse(excelDataList.isEmpty());
        excelDataList.forEach(excelData -> {
            System.out.println("sheetName: " + excelData.getSheetName());
            Assert.assertFalse(excelData.getDataList().isEmpty());
            excelData.getDataList().forEach(System.out::println);
            System.out.println("\n");
        });
    }

    @Test
    public void writeAndReadSimply() {
        String pathname = ExcelToolsTest.class.getResource("/").getPath();
        String[] headers = {"编号", "名字", "手机号", "年龄"};
        List<ExcelProfile> data = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            data.add(ExcelProfile.builder()
                    .no(i + 1)
                    .name("Clive-" + i)
                    .mobile("138001380" + i)
                    .age(i + 20)
                    .build()
            );
        }
        File file = ExcelTools.writeSimply(pathname, headers, data);
        System.out.println("generatedFile:" + file.getAbsolutePath());
        List<ExcelProfile> excelProfiles = ExcelTools.readSimply(file.getAbsolutePath(), ExcelProfile.class, true);
        Assert.assertFalse(excelProfiles.isEmpty());
        excelProfiles.forEach(System.out::println);
    }
}
