package cn.cliveyuan.tools.poi.test.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Clive on 2019/10/28.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExcelProfile {

    private Integer no;
    private String name;
    private String mobile;
    private Integer age;
}
