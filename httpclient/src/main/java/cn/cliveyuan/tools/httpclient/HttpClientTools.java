package cn.cliveyuan.tools.httpclient;

import cn.cliveyuan.tools.common.StringTools;
import cn.cliveyuan.tools.common.ValidateTools;
import cn.cliveyuan.tools.httpclient.bean.HttpClientRequest;
import cn.cliveyuan.tools.httpclient.bean.HttpClientResponse;
import cn.cliveyuan.tools.httpclient.bean.RequestContextTypeEnum;
import cn.cliveyuan.tools.httpclient.bean.RequestMethodEnum;
import cn.cliveyuan.tools.httpclient.client.HttpClient;
import cn.cliveyuan.tools.httpclient.client.OkHttpHttpClient;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * http客户端工具
 *
 * @author Clive Yuan
 * @date 2021/06/08
 */
public class HttpClientTools {

    private static final HttpClient HTTP_CLIENT = new OkHttpHttpClient();
    private static final String DEFAULT_HEADER = "Accept: */*\n" +
            "Accept-Encoding: deflate\n" +
            "Accept-Language: zh-CN,zh;q=0.9\n" +
            "Connection: keep-alive\n" +
            "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36\n" +
            "X-Requested-With: XMLHttpRequest";

    /**
     * 发起get请求
     *
     * @param url 请求地址
     * @return 响应内容
     */
    public static String get(String url) {
        return request(HttpClientRequest.builder().url(url).build()).getResponse();
    }

    /**
     * 发起get请求
     *
     * @param url     请求地址
     * @param headers 请求头
     * @return 响应内容
     */
    public static String get(String url, Map<String, String> headers) {
        return request(HttpClientRequest.builder().url(url).headers(headers).build()).getResponse();
    }

    /**
     * 发起post-form请求
     *
     * @param url      请求地址
     * @param paramMap 参数
     * @return 响应内容
     */
    public static String post(String url, Map<String, Object> paramMap) {
        return request(HttpClientRequest.builder()
                .url(url)
                .method(RequestMethodEnum.POST)
                .params(paramMap)
                .build()).getResponse();
    }

    /**
     * 发起post-form请求
     *
     * @param url      请求地址
     * @param paramMap 参数
     * @param headers  请求头
     * @return 响应内容
     */
    public static String post(String url, Map<String, Object> paramMap, Map<String, String> headers) {
        return request(HttpClientRequest.builder()
                .url(url)
                .method(RequestMethodEnum.POST)
                .headers(headers)
                .params(paramMap)
                .build()).getResponse();
    }

    /**
     * 发起post-json请求
     *
     * @param url         请求地址
     * @param requestBody 请求体
     * @return 响应内容
     */
    public static String postJson(String url, String requestBody) {
        return request(HttpClientRequest.builder()
                .url(url)
                .method(RequestMethodEnum.POST)
                .contextType(RequestContextTypeEnum.JSON)
                .requestBody(requestBody)
                .build()).getResponse();
    }

    /**
     * 发起post-json请求
     *
     * @param url         请求地址
     * @param requestBody 请求体
     * @param headers     请求头
     * @return 响应内容
     */
    public static String postJson(String url, String requestBody, Map<String, String> headers) {
        return request(HttpClientRequest.builder()
                .url(url)
                .method(RequestMethodEnum.POST)
                .contextType(RequestContextTypeEnum.JSON)
                .headers(headers)
                .requestBody(requestBody)
                .build()).getResponse();
    }

    /**
     * 发送请求
     *
     * @param request 入参
     * @return 响应
     */
    public static HttpClientResponse request(HttpClientRequest request) {
        handleRequest(request);
        try {
            return HTTP_CLIENT.request(request);
        } catch (Exception e) {
            if (request.isIgnoreError()) {
                HttpClientResponse response = new HttpClientResponse();
                response.setStatusCode(-1);
                response.setSuccessful(false);
                response.setExceptionMsg(e.getMessage());
                return response;
            }
            throw new RuntimeException(e);
        }
    }

    private static void handleRequest(HttpClientRequest request) {
        ValidateTools.assertValidateEntity(request);
        // 由于lombok创建的Map是不可修改的
        Map<String, Object> params = new HashMap<>();
        if (Objects.nonNull(request.getParams())) {
            params.putAll(request.getParams());
        }
        request.setParams(params);
        Map<String, String> headers = new HashMap<>();
        if (Objects.nonNull(request.getHeaders())) {
            headers.putAll(request.getHeaders());
        }
        request.setHeaders(headers);
        Map<String, String> cookies = new HashMap<>();
        if (Objects.nonNull(request.getCookies())) {
            cookies.putAll(request.getCookies());
        }
        request.setCookies(cookies);

        if (request.isAddDefaultHeaders()) {
            request.getHeaders().putAll(StringTools.str2Map(DEFAULT_HEADER));
        }
        if (StringTools.isNotBlank(request.getKvParams())) {
            request.getParams().putAll(StringTools.str2Map(request.getKvParams()));
        }
        if (StringTools.isNotBlank(request.getKvHeaders())) {
            request.getHeaders().putAll(StringTools.str2Map(request.getKvHeaders()));
        }
        if (StringTools.isNotBlank(request.getKvCookies())) {
            request.getCookies().putAll(StringTools.str2Map(request.getKvCookies()));
        }
        if (!request.getCookies().isEmpty()) {
            request.getHeaders().put("Cookie", StringTools.map2CookieStr(request.getCookies()));
        }
        if (request.isAddDefaultReferer()) {
            request.getHeaders().put("Referer", request.getUrl());
        }
    }
}
