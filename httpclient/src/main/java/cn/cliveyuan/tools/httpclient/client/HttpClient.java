package cn.cliveyuan.tools.httpclient.client;

import cn.cliveyuan.tools.httpclient.bean.HttpClientRequest;
import cn.cliveyuan.tools.httpclient.bean.HttpClientResponse;

/**
 * http客户端接口
 *
 * @author Clive Yuan
 * @date 2021/06/08
 */
public interface HttpClient {

    /**
     * http请求
     *
     * @param request 入参
     * @return 响应
     */
    HttpClientResponse request(HttpClientRequest request);
}
