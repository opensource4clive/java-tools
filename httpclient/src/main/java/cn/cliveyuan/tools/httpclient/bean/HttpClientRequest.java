package cn.cliveyuan.tools.httpclient.bean;

import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.nio.charset.Charset;
import java.time.Duration;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @author Clive Yuan
 * @date 2021/06/08
 */
@Data
@Builder
public class HttpClientRequest {
    /**
     * 请求地址
     */
    @NotBlank(message = "url can't be blank")
    private String url;
    /**
     * 请求方式
     */
    @Builder.Default
    @NotNull(message = "method can't be null")
    private RequestMethodEnum method = RequestMethodEnum.GET;
    /**
     * 内容类型
     */
    @Builder.Default
    @NotNull(message = "contextType can't be null")
    private RequestContextTypeEnum contextType = RequestContextTypeEnum.FORM;
    /**
     * 请求体 (当请求内容类型为JSON时有效)
     */
    @Builder.Default
    private String requestBody = StringUtils.EMPTY;
    /**
     * 参数
     */
    @Singular
    private Map<String, Object> params;
    /**
     * 通过字符串设置请求入参
     * 格式:
     * <pre>
     *     user: clive
     *     pwd:  123456
     * </pre>
     * 每行一对,每对之间用分号分隔对应key-value
     * 会去除前后空格
     */
    private String kvParams;
    /**
     * HEADER
     */
    @Singular
    private Map<String, String> headers;
    /**
     * 通过字符串设置请求Header
     */
    private String kvHeaders;
    /**
     * COOKIE
     */
    @Singular
    private Map<String, String> cookies;
    /**
     * 通过字符串设置请求Cookie
     */
    private String kvCookies;
    /**
     * 连接超时时间 (默认10s)
     */
    private Duration connectTimeout;
    /**
     * 读取超时时间 (默认180s)
     */
    private Duration readTimeout;
    /**
     * 写入超时时间 (默认60s)
     */
    private Duration writeTimeout;
    /**
     * 添加默认Header
     */
    private boolean addDefaultHeaders;
    /**
     * 添加默认Referer (默认为请求地址)
     */
    private boolean addDefaultReferer;
    /**
     * 忽略SSL证书
     */
    private boolean ignoreSslCertificate;
    /**
     * 字符集
     */
    private Charset charset;
    /**
     * 忽略错误 (发生错误时不抛出HttpClientException异常)
     */
    private boolean ignoreError;

    /**
     * 自定义Builder
     */
    private Consumer<OkHttpClient.Builder> customizedBuilderConsumer;
}
