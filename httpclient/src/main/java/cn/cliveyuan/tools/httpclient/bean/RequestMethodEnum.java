package cn.cliveyuan.tools.httpclient.bean;

/**
 * @author Clive Yuan
 * @date 2021/06/08
 */
public enum RequestMethodEnum {
    GET,
    POST,
    PUT,
    DELETE,
    PATCH,
    HEAD,
    OPTIONS,
    TRACE;
}
