package cn.cliveyuan.tools.httpclient.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import okhttp3.MediaType;

/**
 * @author Clive Yuan
 * @date 2021/06/08
 */
@Getter
@AllArgsConstructor
public enum RequestContextTypeEnum {

    FORM(MediaType.get("application/x-www-form-urlencoded;charset=utf-8")),
    JSON(MediaType.get("application/json;charset=utf-8"));

    private final MediaType mediaType;
}
