package cn.cliveyuan.tools.httpclient.bean;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Clive Yuan
 * @date 2021/06/08
 */
@Data
public class HttpClientResponse {
    /**
     * http 状态码
     */
    private int statusCode;
    /**
     * 是否为成功状态
     */
    private boolean isSuccessful;
    /**
     * 响应内容
     */
    private String response;
    /**
     * 响应Header
     */
    private Map<String, String> headers = new HashMap<>();
    /**
     * 响应Cookie
     */
    private Map<String, String> cookies = new HashMap<>();
    /**
     * 报错信息 (当抛异常时且ignoreError=true时有值)
     */
    private String exceptionMsg;
}
