package cn.cliveyuan.tools.httpclient.test;

import cn.cliveyuan.tools.common.AssertTools;
import cn.cliveyuan.tools.httpclient.HttpClientTools;
import cn.cliveyuan.tools.httpclient.bean.HttpClientRequest;
import cn.cliveyuan.tools.httpclient.bean.HttpClientResponse;
import cn.cliveyuan.tools.httpclient.bean.RequestContextTypeEnum;
import cn.cliveyuan.tools.httpclient.bean.RequestMethodEnum;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Clive Yuan
 * @date 2021/06/09
 */
public class HttpClientToolsTest {
    private static final String WORKSTATION_URL = "https://workstation.cliveyuan.cn";

    @Test
    public void get() {
        String response = HttpClientTools.get("https://cliveyuan.cn");
        System.out.println(response);
        AssertTools.notNull(response);
    }

    @Test
    public void post() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", "top");
        map.put("key", "APPKEY");
        String response = HttpClientTools.post("http://v.juhe.cn/toutiao/index", map);
        System.out.println(response);
        AssertTools.notNull(response);
    }

    @Test
    public void postJson() {
        String response = HttpClientTools.postJson(WORKSTATION_URL+"/mgr/admin/login", "{\"account\":\"admin\",\"password\":\"123456\",\"captcha\":{\"id\":1,\"value\":\"111111\"}}");
        System.out.println(response);
        AssertTools.notNull(response);
    }

    @Test
    public void request() {
        Map<String, Object> params = new HashMap<>();
        params.put("username", "clive");
        Map<String, String> headers = new HashMap<>();
        headers.put("x-test", "header");
        Map<String, String> cookies = new HashMap<>();
        cookies.put("pid", "cookie");
        HttpClientResponse response = HttpClientTools.request(HttpClientRequest.builder()
                .url(WORKSTATION_URL +"/mgr/app-tool/page")
                .method(RequestMethodEnum.POST)
                .contextType(RequestContextTypeEnum.JSON)
                .requestBody("{\"pageParameter\":{\"page\":1,\"limit\":30},\"record\":{\"app\":9}}")
                .params(params)
                .headers(headers)
                .cookies(cookies)
                .kvParams("pwd: 11223344")
                .kvHeaders("X-Token: 84459edd573f52b7b515d454be708c2e")
                .kvCookies("kv_pid: cookie2")
                .addDefaultReferer(true)
                .addDefaultHeaders(true)
                .build());
        AssertTools.isTrue(response.isSuccessful());
        System.out.println("response: " + response.getResponse());
        System.out.println("headers: " + response.getHeaders());
        AssertTools.notNull(response.getResponse());
    }

    @Test
    public void request2() {
        Map<String, Object> params = new HashMap<>();
        params.put("username", "clive");
        Map<String, String> headers = new HashMap<>();
        headers.put("x-test", "header");
        Map<String, String> cookies = new HashMap<>();
        cookies.put("pid", "cookie");
        // Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", 7890));
        HttpClientResponse response = HttpClientTools.request(HttpClientRequest.builder()
                .url(WORKSTATION_URL +"/mgr/app-tool/page")
                .method(RequestMethodEnum.POST)
                .contextType(RequestContextTypeEnum.JSON)
                .requestBody("{\"pageParameter\":{\"page\":1,\"limit\":30},\"record\":{\"app\":9}}")
                .params(params)
                .param("add", "true2")
                .headers(headers)
                .cookies(cookies)
                .kvParams("pwd: 11223344")
                .kvHeaders("X-Token: 84459edd573f52b7b515d454be708c2e")
                .kvCookies("kv_pid: cookie2")
                .addDefaultReferer(true)
                .addDefaultHeaders(true)
                // .customizedBuilderConsumer(builder -> builder.proxy(proxy))
                .build());
        AssertTools.isTrue(response.isSuccessful());
        System.out.println("response: " + response.getResponse());
        System.out.println("headers: " + response.getHeaders());
        AssertTools.notNull(response.getResponse());
    }

}
