package cn.cliveyuan.tools.web.bean;

/**
 * Created by Clive on 2019/06/16.
 */
public enum CaptchaGenerateType {

    /**
     * 会话
     */
    SESSION,

    /**
     * 缓存
     */
    CACHE

}
