package cn.cliveyuan.tools.web.bean;

import lombok.Builder;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * Created by Clive on 2019/06/25.
 */
@Data
@Builder
public class FileUploadRequest implements Serializable {

    /**
     * 上传的文件
     */
    private MultipartFile multipartFile;

    /**
     * 请求
     */
    private HttpServletRequest request;

    /**
     * 绝对保存地址
     */
    private String absoluteSavePath;
    /**
     * 相对保存地址
     */
    private String relativeSavePath;

    /**
     * 文件名 (不带扩展名, 为空则系统生成)
     */
    private String fileName;

    /**
     * 允许的文件类型, 多个以逗号分隔 不区分大小写, 为null则不限制
     */
    private String allowedExtensions;

}
