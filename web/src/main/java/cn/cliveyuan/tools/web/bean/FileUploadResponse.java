package cn.cliveyuan.tools.web.bean;

import lombok.Builder;
import lombok.Data;

/**
 * 文件上传响应
 *
 * @author clive
 * Created on 2018/07/27
 * @since 1.0
 */
@Data
@Builder
public class FileUploadResponse {

    /**
     * 文件的绝对路径
     */
    private String fileRealPath;

    /**
     * 文件的web相对路径
     */
    private String webRelativePath;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 扩展名
     */
    private String extension;

    /**
     * 文件大小 (单位byte)
     */
    private Long length;
}
