package cn.cliveyuan.tools.web.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 验证码
 *
 * @author clive
 * Created on 2019/06/16
 * @since 2.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Captcha implements Serializable {

    private String id;
    private String value;
}
