package cn.cliveyuan.tools.web.exception;

/**
 * 验证码异常
 *
 * @author clive
 * Created on 2018/07/27
 * @since 1.0
 */
public class CaptchaException extends IllegalArgumentException {

    public CaptchaException(int code, String message) {
        super(message);
        this.code = code;
    }

    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static CaptchaException captchaIsNull() {
        return new CaptchaException(1000, "验证码为空");
    }

    public static CaptchaException idIsEmpty() {
        return new CaptchaException(1001, "验证码错误");
    }

    public static CaptchaException valueIsEmpty() {
        return new CaptchaException(1002, "验证码为空");
    }

    public static CaptchaException notMatch() {
        return new CaptchaException(1003, "验证码错误");
    }

    public static CaptchaException notInSession() {
        return new CaptchaException(1004, "验证码错误");
    }

    public static CaptchaException parseJsonError() {
        return new CaptchaException(1005, "验证码解析异常");
    }
}
