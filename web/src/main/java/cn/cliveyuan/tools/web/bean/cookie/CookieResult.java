package cn.cliveyuan.tools.web.bean.cookie;

import lombok.Builder;
import lombok.Data;

import javax.servlet.http.Cookie;

/**
 * @author Clive Yuan
 * @date 2021/09/28
 */
@Data
@Builder
public class CookieResult {
    private String value;
    private Cookie cookie;
}
