package cn.cliveyuan.tools.web.bean.cookie;

import lombok.Data;
import lombok.experimental.SuperBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

/**
 * @author Clive Yuan
 * @date 2021/09/28
 */
@Data
@SuperBuilder
public class CookieParam {
    @NotNull(message = "request is required")
    private HttpServletRequest request;
    @NotNull(message = "response is required")
    private HttpServletResponse response;
    @NotBlank(message = "cookieName is required")
    private String cookieName;

    @Override
    public String toString() {
        return "CookieParam{" +
                "cookieName='" + cookieName + '\'' +
                '}';
    }
}
