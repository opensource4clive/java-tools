package cn.cliveyuan.tools.web.bean.cookie;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.time.Duration;

/**
 * @author Clive Yuan
 * @date 2021/09/28
 */
@Data
@SuperBuilder
public class SetCookieParam extends CookieParam {
    /**
     * Cookie值
     */
    @Builder.Default
    private String cookieValue = "";
    /**
     * 存活时间
     */
    private Duration maxAge;
    /**
     * 永久有效
     */
    private boolean forever;
    /**
     * 子域名可共享
     * <p>
     * <pre>
     *      如当前域名为: www.cliveyuan.cn
     *      false: cookie.domain = www.cliveyuan.cn
     *      true: cookie.domain = .cliveyuan.cn
     * </pre>
     * </p>
     */
    private boolean subdomainSharable;
    /**
     * 设置为HttpOnly时,cookie只能后端操作,前端无法操作
     */
    private boolean httpOnly;
    /**
     * Cookie路径
     */
    @Builder.Default
    private String path = "/";
    /**
     * 安全性设置, 设置之后只能通过安全协议访问,如https或者SSL
     */
    private boolean secure;
    /**
     * 备注
     */
    private String comment;
    /**
     * 版本号
     */
    private int version;
}
