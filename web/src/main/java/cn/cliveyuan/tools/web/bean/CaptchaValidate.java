package cn.cliveyuan.tools.web.bean;

import lombok.Builder;
import lombok.Data;
import org.springframework.cache.CacheManager;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * Created by Clive on 2019/06/16.
 */
@Data
@Builder
public class CaptchaValidate implements Serializable {

    /**
     * 验证码生成类型 (必须)
     */
    private CaptchaGenerateType generateType;

    /**
     * http 请求 (生成类型为SESSION时必须)
     */
    private HttpServletRequest request;

    /**
     * 缓存管理器 (生成类型为CACHE时必须)
     */
    private CacheManager cacheManager;
    /**
     * 缓存命名空间
     */
    private String cacheNamespace;

    /**
     * 验证码存储名 (不传则采用默认值, 与生成时保持一致)
     */
    private String name;

    /**
     * 输入的验证码 (必须)
     */
    private Captcha captcha;

    /**
     * 当验证失败时抛CaptchaException异常
     */
    private boolean throwExceptionWhenError;


}
