package cn.cliveyuan.tools.web.bean;

/**
 * 验证码类型
 * <p>
 * Created by Clive on 2020/02/17.
 */
public enum CaptchaType {

    /**
     * 字母数字
     */
    LETTER_NUM,

    /**
     * 数学加法
     */
    MATH_ADD

}
