package cn.cliveyuan.tools.web.bean;

import lombok.Builder;
import lombok.Data;
import org.springframework.cache.CacheManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * Created by Clive on 2019/06/16.
 */
@Data
@Builder
public class CaptchaRequest implements Serializable {

    /**
     * 验证码类型 (非必须,默认是 'LETTER_NUM')
     */
    @Builder.Default
    private CaptchaType captchaType = CaptchaType.LETTER_NUM;

    /**
     * 验证码生成类型 (必须)
     */
    private CaptchaGenerateType generateType;

    /**
     * http 请求 (生成类型为SESSION时必须)
     */
    private HttpServletRequest request;

    /**
     * 缓存管理器 (生成类型为CACHE时必须)
     */
    private CacheManager cacheManager;

    /**
     * 缓存命名空间 (生成类型为CACHE时必须)
     */
    private String cacheNamespace;

    /**
     * http 响应 (必须, 用于生成验证码)
     */
    private HttpServletResponse response;

    /**
     * 验证码id (不传则自动生成)
     */

    private String id;
    /**
     * 验证码存储名 (不传则采用默认值, 与验证时保持一致)
     */

    private String name;
    /**
     * 验证码位数 (不传则采用默认值)
     */

    private int length;
    /**
     * 宽度 (不传则采用默认值)
     */

    private int width;
    /**
     * 高度 (不传则采用默认值)
     */
    private int height;

    /**
     * 数学加法 开始
     */
    @Builder.Default
    private int mathAddStart = 1;

    /**
     * 数学加法 结束
     */
    @Builder.Default
    private int mathAddEnd = 10;

}
