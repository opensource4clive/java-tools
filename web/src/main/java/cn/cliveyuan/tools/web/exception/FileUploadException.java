package cn.cliveyuan.tools.web.exception;

import cn.cliveyuan.tools.common.exception.BaseException;

/**
 * 文件上传异常
 *
 * @author clive
 * Created on 2018/07/27
 * @since 1.0
 */
public class FileUploadException extends BaseException {

    public FileUploadException(int code, String message) {
        super(code, message);
    }

    public static FileUploadException uploadPathNotExist() {
        return new FileUploadException(1000, "upload path not exist");
    }

    public static FileUploadException notSupport(String message) {
        return new FileUploadException(1001, message);
    }

    public static FileUploadException failToMkdirs(String folder) {
        return new FileUploadException(1002, "fail to create folder(s): " + folder);
    }

    public static FileUploadException fileEmpty() {
        return new FileUploadException(1003, "file is empty");
    }

    public static FileUploadException unKnowError() {
        return new FileUploadException(1003, "unKnow Error");
    }
}
