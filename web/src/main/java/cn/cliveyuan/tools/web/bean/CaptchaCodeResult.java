package cn.cliveyuan.tools.web.bean;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Clive on 2020/02/17.
 */
@Data
@Builder
public class CaptchaCodeResult implements Serializable {
    private String captchaContent;
    private String verifyContent;
}
