package cn.cliveyuan.tools.web.test;

import cn.cliveyuan.tools.web.CookieTools;
import cn.cliveyuan.tools.web.bean.cookie.CookieParam;
import cn.cliveyuan.tools.web.bean.cookie.SetCookieParam;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.Cookie;
import java.util.Arrays;

/**
 * @author Clive Yuan
 * @date 2021/09/28
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestApplication.class)
public class CookieToolsTest {

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Before
    public void setup() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
    }

    @Test
    public void testCookie() {
        String name = "cookieTest";
        String value = "clive";
        CookieTools.set(SetCookieParam.builder()
                .request(request).response(response).cookieName(name)
                .cookieValue(value)
                .build());

        // response cookie -> request cookie
        request.setCookies(response.getCookies());

        String val = CookieTools.getValue(CookieParam.builder()
                .request(request).response(response).cookieName(name)
                .build());
        Assert.assertEquals(value, val);

        CookieTools.delete(CookieParam.builder()
                .request(request).response(response).cookieName(name)
                .build());

        // response cookie -> request cookie
        request.setCookies();

        val = CookieTools.getValue(CookieParam.builder()
                .request(request).response(response).cookieName(name)
                .build());
        Assert.assertNull(val);
    }
}
